{{--
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Beneficiaries - MTN Foundation Scholarship</title>

    <!-- Main Styles -->
    <link rel="stylesheet" href="assets/styles/style.min.css">
    <link rel="stylesheet" href="assets/styles/style-dark.min.css">
    <link rel="stylesheet" href="assets/styles/color/yellow.min.css">

    <!-- mCustomScrollbar -->
    <link rel="stylesheet" href="assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css">

    <!-- Waves Effect -->
    <link rel="stylesheet" href="assets/plugin/waves/waves.min.css">

    <!-- Sweet Alert -->
    <link rel="stylesheet" href="assets/plugin/sweet-alert/sweetalert.css">

    <!-- Data Tables -->
    <link rel="stylesheet" href="assets/plugin/datatables/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="assets/plugin/datatables/extensions/Responsive/css/responsive.bootstrap.min.css">

</head>
--}}

@extends('layouts.admin.main')

@section('page_title')
    Beneficiaries
@endsection

@section('content')
    <div class="row small-spacing">
        <div class="col-xs-12">
            <div class="box-content">
                <h4 class="box-title">Default</h4>
                <!-- /.box-title -->
                <div class="dropdown js__drop_down">
                    <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                    <ul class="sub-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else there</a></li>
                        <li class="split"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    <!-- /.sub-menu -->
                </div>
                <!-- /.dropdown js__dropdown -->
                <table id="example" class="table table-striped table-bordered display" style="width:100%">
                    <thead>
                    <tr>
                        <th style="width:30%;">Name</th>
                        <th>Scholarship Type</th>
                        <th>Location</th>
                        <th>Age</th>
                        <th>Start date</th>
                        <th>Completion Date</th>
                        <th>Uploaded Documents</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th style="width:30%;">Name</th>
                        <th>Scholarship Type</th>
                        <th>Location</th>
                        <th>Age</th>
                        <th>Start date</th>
                        <th>Completion Date</th>
                        <th style="width:18%;">Uploaded Documents</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Tiger
                                Nixon</a></td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Garrett
                                Winters</a></td>
                        <td>Accountant</td>
                        <td>Tokyo</td>
                        <td>63</td>
                        <td>2011/07/25</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Ashton
                                Cox</a></td>
                        <td>Junior Technical Author</td>
                        <td>San Francisco</td>
                        <td>66</td>
                        <td>2009/01/12</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Cedric Kelly</a>
                        </td>
                        <td>Senior Javascript Developer</td>
                        <td>Edinburgh</td>
                        <td>22</td>
                        <td>2012/03/29</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Airi
                                Satou</a></td>
                        <td>Accountant</td>
                        <td>Tokyo</td>
                        <td>33</td>
                        <td>2008/11/28</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Brielle
                                Williamson</a></td>
                        <td>Integration Specialist</td>
                        <td>New York</td>
                        <td>61</td>
                        <td>2012/12/02</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Herrod
                                Chandler</a></td>
                        <td>Sales Assistant</td>
                        <td>San Francisco</td>
                        <td>59</td>
                        <td>2012/08/06</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Rhona
                                Davidson</a></td>
                        <td>Integration Specialist</td>
                        <td>Tokyo</td>
                        <td>55</td>
                        <td>2010/10/14</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Colleen Hurst</a>
                        </td>
                        <td>Javascript Developer</td>
                        <td>San Francisco</td>
                        <td>39</td>
                        <td>2009/09/15</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Sonya
                                Frost</a></td>
                        <td>Software Engineer</td>
                        <td>Edinburgh</td>
                        <td>23</td>
                        <td>2008/12/13</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Jena
                                Gaines</a></td>
                        <td>Office Manager</td>
                        <td>London</td>
                        <td>30</td>
                        <td>2008/12/19</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Quinn
                                Flynn</a></td>
                        <td>Support Lead</td>
                        <td>Edinburgh</td>
                        <td>22</td>
                        <td>2013/03/03</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Charde
                                Marshall</a></td>
                        <td>Regional Director</td>
                        <td>San Francisco</td>
                        <td>36</td>
                        <td>2008/10/16</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Haley Kennedy</a>
                        </td>
                        <td>Senior Marketing Designer</td>
                        <td>London</td>
                        <td>43</td>
                        <td>2012/12/18</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Tatyana
                                Fitzpatrick</a></td>
                        <td>Regional Director</td>
                        <td>London</td>
                        <td>19</td>
                        <td>2010/03/17</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Michael Silva</a>
                        </td>
                        <td>Marketing Designer</td>
                        <td>London</td>
                        <td>66</td>
                        <td>2012/11/27</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Paul Byrd</a>
                        </td>
                        <td>Chief Financial Officer (CFO)</td>
                        <td>New York</td>
                        <td>64</td>
                        <td>2010/06/09</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Gloria Little</a>
                        </td>
                        <td>Systems Administrator</td>
                        <td>New York</td>
                        <td>59</td>
                        <td>2009/04/10</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Bradley Greer</a>
                        </td>
                        <td>Software Engineer</td>
                        <td>London</td>
                        <td>41</td>
                        <td>2012/10/13</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Dai Rios</a>
                        </td>
                        <td>Personnel Lead</td>
                        <td>Edinburgh</td>
                        <td>35</td>
                        <td>2012/09/26</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Jenette
                                Caldwell</a></td>
                        <td>Development Lead</td>
                        <td>New York</td>
                        <td>30</td>
                        <td>2011/09/03</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Yuri
                                Berry</a></td>
                        <td>Chief Marketing Officer (CMO)</td>
                        <td>New York</td>
                        <td>40</td>
                        <td>2009/06/25</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Caesar Vance</a>
                        </td>
                        <td>Pre-Sales Support</td>
                        <td>New York</td>
                        <td>21</td>
                        <td>2011/12/12</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Doris Wilder</a>
                        </td>
                        <td>Sales Assistant</td>
                        <td>Sidney</td>
                        <td>23</td>
                        <td>2010/09/20</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Angelica
                                Ramos</a></td>
                        <td>Chief Executive Officer (CEO)</td>
                        <td>London</td>
                        <td>47</td>
                        <td>2009/10/09</td>
                        <td>$1,200,000</td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Gavin
                                Joyce</a></td>
                        <td>Developer</td>
                        <td>Edinburgh</td>
                        <td>42</td>
                        <td>2010/12/22</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Jennifer
                                Chang</a></td>
                        <td>Regional Director</td>
                        <td>Singapore</td>
                        <td>28</td>
                        <td>2010/11/14</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Brenden
                                Wagner</a></td>
                        <td>Software Engineer</td>
                        <td>San Francisco</td>
                        <td>28</td>
                        <td>2011/06/07</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Fiona
                                Green</a></td>
                        <td>Chief Operating Officer (COO)</td>
                        <td>San Francisco</td>
                        <td>48</td>
                        <td>2010/03/11</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Shou Itou</a>
                        </td>
                        <td>Regional Marketing</td>
                        <td>Tokyo</td>
                        <td>20</td>
                        <td>2011/08/14</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Michelle
                                House</a></td>
                        <td>Integration Specialist</td>
                        <td>Sidney</td>
                        <td>37</td>
                        <td>2011/06/02</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Suki
                                Burks</a></td>
                        <td>Developer</td>
                        <td>London</td>
                        <td>53</td>
                        <td>2009/10/22</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Prescott
                                Bartlett</a></td>
                        <td>Technical Author</td>
                        <td>London</td>
                        <td>27</td>
                        <td>2011/05/07</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="profile.html" class="">
                                <span class="avatar "><img src="http://placehold.it/128x128" alt=""></span>Gavin Cortez</a>
                        </td>
                        <td>Team Leader</td>
                        <td>San Francisco</td>
                        <td>22</td>
                        <td>2008/10/26</td>
                        <td>2021/12/16</td>
                        <td><a
                                class="btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-eye"></i>View All</a>

                            <a class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                    class="ico fa fa-download"></i>Download</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-xs-12 -->
    </div>
@endsection
