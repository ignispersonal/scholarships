@extends('layouts.admin.main')

@section('page_title')
    Add New Scholarship Batch
@endsection

@section('custom_css')

{{-- Dropify --}}
<link rel="stylesheet" href="{{ asset('assets/plugin/dropify/css/dropify.min.css') }}">

<!-- Datepicker -->
<link rel="stylesheet" href="{{ asset('assets/plugin/datepicker/css/bootstrap-datepicker.min.css') }}">

@endsection

@section('content')
    <div class="row small-spacing">
        <form data-toggle="validator" method="post" action="{{ route('batches.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="col-md-7 col-xs-12 ">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-content bordered bordered-all primary margin-bottom-20 ">
                            <h4 class="box-title">New Scholarship Batch</h4>
                                {{--Scholarship Name--}}
                                <div class="form-group has-feedback">
                                    <label for="inputTitle" class="control-label">Title</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-ticket ico"></i></span>
                                        <input type="text" pattern="^[_A-z]{1,}(\w|\d|-|_)*" maxlength="25" class="form-control"
                                               id="inputTitle" name="title" placeholder="Title for batch" data-error="Sorry, invalid input"
                                               value="{{ old('title') }}" required>
                                    </div>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors">
                                        @if ($errors->has('title'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                {{--Scholarship Description--}}
                                <div class="form-group has-feedback">
                                    <label for="inputDescription" class="control-label">Description</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-edit ico"></i></span>
                                        <textarea type="text" class="form-control"
                                               id="inputDescription" name="description" placeholder=",
                                               Tell us about the new scholarship programme" data-error="Sorry, you can't leave this blank."
                                                  required>{{ old('description') }}</textarea>
                                    </div>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors">
                                        @if ($errors->has('description'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group has-feedback">
                                    <label for="inputSType">Scholarship Type</label>
                                    <select id="inputSType" class="form-control" name="scholarship_type_id" data-error="Sorry, you have to select one." required>
                                        <option value="">Select</option>
                                        @foreach ($scholarshipTypes as $key => $value)
                                            <option value="{{ $key }}" {{ old('scholarship_type_id') == $key ? 'selected' : ''}}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors">
                                        @if ($errors->has('scholarship_type_id'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('scholarship_type_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group has-feedback">
                                    <label for="date-range">Scholarship Start and End Date</label>
                                    <div class="input-daterange input-group" id="date-range">
                                        <span class="input-group-addon bg-primary text-white"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control" name="start_date" value="{{ old('start_date') }}" required/>
                                        <span class="input-group-addon bg-primary text-white">to</span>
                                        <input type="text" class="form-control" name="end_date" value="{{ old('end_date') }}" required/>
                                    </div>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors">
                                        @if ($errors->has('start_date'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('start_date') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('end_date'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('end_date') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Create</button>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection

@section('scripts')

    {{-- Validator --}}
    <script src="{{ asset('assets/plugin/validator/validator.min.js') }}"></script>

    <!-- Datepicker -->
    <script src="{{ asset('assets/plugin/datepicker/js/bootstrap-datepicker.min.js') }}"></script>

    <!-- Demo Scripts -->
    <script src="{{ asset('assets/scripts/form.demo.js') }}"></script>

@endsection
