@extends('layouts.admin.main')

@section('page_title')
    Batches
@endsection

@section('custom_css')
    <!-- Data Tables -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/datatables/media/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugin/datatables/extensions/Responsive/css/responsive.bootstrap.min.css') }} ">

@endsection

@section('content')
    <div class="row small-spacing">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box-content card" style="padding: 1%">
                        <form action="#" class="">
                            <div class="row form-horizontal">
                                <div class="col-lg-2 col-md-2 col-sm-5 " >
                                    <a  href="{{ route('batches.create') }}" class="btn-icon btn-icon-left btn btn-info waves-effect waves-light">
                                        <i class="ico fa fa-plus"></i>Add New Batch</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row small-spacing">

        @if(isset($batches))
            <div class="col-xs-12">
                <div class="box-content">

                    <table class="table table-striped table-bordered display" style="width:100%">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Scholarship Type</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>No. Applied</th>
                            <th>No. Awarded</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Scholarship Type</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>No. Applied</th>
                            <th>No. Awarded</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                        <tbody>
                            @foreach($batches as $batch)
                                <tr>
                                    <td>{{ $batch->title }}</td>
                                    <td>{{ $batch->description }}</td>
                                    <td>{{ $batch->scholarshipType['name'] }}</td>
                                    <td>{{ $batch->start_date->format('d M Y') }}</td>
                                    <td>{{ $batch->end_date->format('d M Y') }}</td>
                                    <td>{{ $batch->no_applied }}</td>
                                    <td>{{ $batch->no_awarded }}</td>
                                    <td>{{ $batch->status }}</td>
                                    <td><form method="post" action="{{ route('batches.destroy', $batch->id) }}">
                                            @method('delete')
                                            @csrf
                                        <a href="{{ route('batches.edit',$batch->id) }}"
                                            class="btn btn-icon btn-icon-left btn-warning btn-xs waves-effect waves-light btn-rounded"><i
                                                class="ico fa fa-pencil"></i>Update</a>

                                        <button type="submit" class="btn btn-icon btn-icon-left btn-danger btn-xs waves-effect waves-light btn-rounded"><i
                                                class="ico fa fa-ban"></i>Delete</button></form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-content -->
            </div>

        @else
        //
        @endif
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugin/datatables/media/js/jquery.dataTables.min.js') }}"></script>

    <script src="{{ asset('assets/plugin/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>

    <script src="{{ asset('assets/scripts/datatables.demo.min.js') }} "></script>

@endsection
