@extends('layouts.admin.main')

@section('page_title')
    Applications
@endsection

@section('content')
    <div class="row small-spacing">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box-content card" style="padding: 1%">
                        <form action="{{route('applications.index')}}" method="get" class="">
                            <div class="row form-horizontal">
                                <div class="col-lg-5 col-md-6 col-sm-12">
                                    <label for="SType">Scholarship Type</label>
                                    <select id="SType" class="form-control" name="scholarshiptype">
                                        <option value="">Select</option>
                                        @isset($scholarshipTypes)
                                            @foreach ($scholarshipTypes as $key => $value)
                                                <option value="{{ $key }}" {{ old('scholarshiptype') == $key ? 'selected' : ''}}>{{ $value }}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                </div>
                                <div class="col-lg-1 col-md-2 col-sm-3 " >
                                    <p> <br/></p>
                                    <button type="button"  class="btn btn-success waves-effect waves-light">Go</button>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-5 " >
                                    <p> <br/></p>
                                    <a  href="{{route('applications.create')}}" class="btn-icon btn-icon-left btn btn-info waves-effect waves-light"><i class="ico fa fa-user-plus"></i>New Application</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-content card -->
                </div>
            </div>
        </div>
        <!-- /.col-xs-12 -->
    </div>
    <div class="row small-spacing">

        @if(isset($applications))
            <div class="col-xs-12">
                <div class="box-content">
                    <table id="applications" class="table table-striped table-bordered display" style="width:100%">
                        <thead>
                        <tr>
                            <th style="width:25%;">Name</th>
                            <th>Scholarship Type</th>
                            <th>Institution</th>
                            <th>Programme</th>
                            <th>Level</th>
                            <th>Gender</th>
                            <th>Submitted</th>
                            <th>Status</th>boot
                            <th>Uploaded Documents</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th style="width:25%;">Name</th>
                            <th>Scholarship Type</th>
                            <th>Institution</th>
                            <th>Programme</th>
                            <th>Level</th>
                            <th>Gender</th>
                            <th>Submitted</th>
                            <th>Status</th>
                            <th>Uploaded Documents</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($applications as $application)
                            <tr>
                            <td><a href="{{route('applications.show',$application->id)}}" class="">
                                    <span class="avatar"><img src="{{ asset('storage/'.$application->photo) }}" alt=""></span>{{$application->getFullName()}}</a>
                            </td>
                            <td>{{ $application->scholarshiptype }}</td>
                            <td>{{ $application->institution }}</td>
                            <td>{{ $application->programme }}</td>
                            <td>{{ $application->programme_year }}</td>
                            <td>{{ $application->gender }}</td>
                            <td>{{ $application->created_at }}</td>
                            <td class="{{ $application->statusColor() }}">{{ $application->status }}</td>
                            <td>
                                <a href="{{ route('applications.downloadall', $application->id) }}" class="btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light btn-rounded"><i
                                        class="ico fa fa-download"></i>Download All</a>
                            </td>
                            <td><form method="post" action="{{ route('applications.destroy', $application->id) }}">
                                    @method('delete')
                                    @csrf
                                    <a href="{{ route('applications.edit',$application->id) }}"
                                       class="btn btn-icon btn-icon-left btn-warning btn-xs waves-effect waves-light btn-rounded"><i
                                            class="ico fa fa-pencil"></i>Update</a>

                                    <button type="submit" class="btn btn-icon btn-icon-left btn-danger btn-xs waves-effect waves-light btn-rounded"><i
                                            class="ico fa fa-ban"></i>Delete</button></form>
                            </td>
                        </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-content -->
            </div>


        @else
            <div class="alert alert-info">
                <ul>
                    <li>{{ "Sorry, no records found!" }}</li>
                </ul>
            </div>
        @endif
    </div>
@endsection
