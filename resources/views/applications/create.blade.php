@extends('layouts.admin.main')

@section('page_title')
    New Application
@endsection

@section('custom_css')

    <!-- Dropify -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/dropify/css/dropify.min.css') }}">

    <!-- Datepicker -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/datepicker/css/bootstrap-datepicker.min.css') }}">

@endsection

@section('content')
    <div class="row small-spacing">
        <form data-toggle="validator" method="post" action="{{ route('applications.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="col-md-3 col-xs-12">
                <div class="box-content bordered bordered-all primary margin-bottom-20">
                    <div class="profile-avatar">
                        <h4 class="box-title">Photo</h4>

                    </div>
                    <!-- /.box-content -->
                    <div class="profile-avatar has-feedback">
                        <input type="file" id="input-file-to-destroy" class="dropify" name="photo" data-allowed-formats="portrait square"
                               data-max-file-size="1M" data-max-height="450" data-error="Sorry, you can't leave this blank"  required/>
                        <p class="help margin-top-10">Only portrait or square images, 1M max and 450 max-height.</p>
                    </div>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors">
                        @if ($errors->has('photo'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('photo') }}</strong>
                            </span>
                        @endif
                    </div>
                    <!-- .profile-avatar -->
                </div>
                @if(isset($batches))
                <div class="box-content bordered bordered-all primary margin-bottom-20">
                    <div class="profile-avatar">
                        <h4 class="box-title">Batch</h4>

                        <div class="form-group has-feedback">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa ico"></i></span>
                                <select class="form-control" name="batches_id" data-error="Sorry, you have to select one." required>
                                    <option value="">Select</option>
                                    @foreach ($batches as $key => $value)
                                        <option value="{{ $key }}" {{ old('batches_id') == $key ? 'selected' : ''}}>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="help-block with-errors" >
                                @if ($errors->has('batches_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('batches_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="box-content bordered bordered-all primary margin-bottom-20">
                    <div class="profile-avatar">
                        <h4 class="box-title">Upload Supporting Documents</h4>
                        {{--Transcript--}}
                        <div class="form-group has-feedback">
                            <label for="exampleInputFile">Transcript</label>
                            <input type="file" id="exampleInputFile" name="transcript">
                            <p class="help-block">file size must not exceed 1MB.</p>
                            <div class="help-block with-errors" >
                                @if ($errors->has('transcript'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('transcript') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                        {{--WASSCE--}}
                        <div class="form-group has-feedback">
                            <label for="exampleInputFile">WASSCE Results</label>
                            <input type="file" id="exampleInputFile" name="wasce">
                            <p class="help-block">file size must not exceed 1MB.</p>
                            <div class="help-block with-errors" >
                                @if ($errors->has('wassce'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('wassce') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                        {{--Admission Letter--}}
                        <div class="form-group has-feedback">
                            <label for="exampleInputFile">Admission / Acceptance Letter</label>
                            <input type="file" id="exampleInputFile" name="adm_letter">
                            <p class="help-block">file size must not exceed 1MB.</p>
                            <div class="help-block with-errors" >
                                @if ($errors->has('adm_letter'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('adm_letter') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                        {{--Testimonial Letter--}}
                        <div class="form-group has-feedback">
                            <label for="exampleInputFile">Testimonial</label>
                            <input type="file" id="exampleInputFile" name="testimonial">
                            <p class="help-block">file size must not exceed 1MB.</p>
                            <div class="help-block with-errors" >
                                @if ($errors->has('testimonial'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('testimonial') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                        {{--Citizenship--}}
                        <div class="form-group has-feedback">
                            <label for="exampleInputFile">Proof of Citizenship</label>
                            <input type="file" id="exampleInputFile" name="citizenship">
                            <p class="help-block">file size must not exceed 1MB.</p>
                            <div class="help-block with-errors" >
                                @if ($errors->has('citizenship'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('citizenship') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-7 col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-content bordered bordered-all primary margin-bottom-20 ">
                            <h4 class="box-title">Personal Information</h4>
                            <div class="form-group has-feedback">
                                <label for="inputFname" class="control-label">First Name</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user ico"></i></span>
                                    <input type="text" pattern="^[_A-z]{1,}$" maxlength="15" class="form-control"
                                           id="inputFname" name="fname" placeholder="First Name" data-error="Sorry, you can't leave this blank"
                                           value="{{ old('fname') }}" required>
                                </div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="inputLname" class="control-label">Last Name</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user ico"></i></span>
                                    <input type="text" pattern="^[_A-z]{1,}$" maxlength="15" class="form-control"
                                           id="inputLname" name="lname" placeholder="Last Name" data-error="Sorry, you can't leave this blank"
                                           value="{{ old('lname') }}" required>
                                </div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="form-group col-sm-6 has-feedback">
                                        <label for="inputDOB" class="control-label">Date of Birth</label>

                                        <div class="input-group datepicker" id="inputDOB">
                                            <span class="input-group-addon"><i class="fa fa-calendar ico"></i></span>
                                            <input data-minlength="6" class="form-control" id="datepicker"
                                                   name="date_of_birth" placeholder="dd/mm/yyyy"
                                                   value="{{ old('date_of_birth') }}" required>
                                        </div>
                                        <div class="help-block with-errors" >
                                            @if ($errors->has('date_of_birth'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('date_of_birth') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6 has-feedback">
                                        <label for="inputGender" class="control-label">Gender</label>
                                        <div class="input-group datepicker" id="inputGender">
                                            <span class="input-group-addon"><i class="fa fa-gender ico"></i></span>
                                            <select class="form-control" name="gender" data-error="Sorry, you have to select one." required>
                                                <option value="">Select</option>
                                                <option value="male" {{ old('gender') == 'male' ? 'selected' : ''}}>male</option>
                                                <option value="female" {{ old('gender') == 'female' ? 'selected' : ''}}>female</option>
                                            </select>
                                        </div>
                                        <div class="help-block with-errors" >
                                            @if ($errors->has('gender'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('gender') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="form-group col-sm-6 has-feedback">
                                        <label for="inputEmp" class="control-label">Employment Status</label>
                                        <div class="input-group" id="inputEmp">
                                            <span class="input-group-addon"><i class="fa fa-money ico"></i></span>
                                            <select class="form-control" name="employment_status" data-error="Sorry, you have to select one." required>
                                                <option value="">Select</option>
                                                @foreach (\App\Applications::empStatus() as $value)
                                                    <option value="{{ $value }}" {{ old('employment_status') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="help-block with-errors" >
                                            @if ($errors->has('employment_status'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('employment_status') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6 has-feedback">
                                        <label for="inputMarital" class="control-label">Marital Status</label>
                                        <div class="input-group" id="inputMarital">
                                            <span class="input-group-addon"><i class="fa fa-ring ico"></i></span>
                                            <select class="form-control" name="marital_status" data-error="Sorry, you have to select one." required>
                                                <option value="">Select</option>
                                                @foreach (\App\Applications::maritalStatus() as $value)
                                                    <option value="{{ $value }}" {{ old('marital_status') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="help-block with-errors" >
                                            @if ($errors->has('marital_status'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('marital_status') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="inputNat" class="control-label">Nationality</label>
                                <div class="input-group datepicker" id="inputNat">
                                    <span class="input-group-addon"><i class="fa fa-gender ico"></i></span>
                                    <select class="form-control" name="nationality" data-error="Sorry, you have to select one." required>
                                        <option value="">Select</option>
                                        @foreach (getNationalityList() as $value)
                                            <option value="{{ $value }}" {{ old('nationality') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="help-block with-errors" >
                                    @if ($errors->has('nationality'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nationality') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-content bordered bordered-all primary margin-bottom-20 ">
                            <h4 class="box-title">Personal Contact Information</h4>
                            <!-- /.box-title -->
                            <div class="form-group has-feedback">
                                <label for="inputEmail" class="control-label">Email</label>
                                <div class="input-group">
                                    <span class="input-group-addon">@</span>
                                    <input type="email" class="form-control" id="inputEmail" placeholder="Email"
                                           name="email" data-error="Bruh, that email address is invalid"
                                           value="{{ old('email') }}" required>
                                </div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group has-feedback">
                                    <label for="inputPhone" class="control-label">Phone</label>
                                    <div class="input-group" id="inputPhone">
                                        <span class="input-group-addon"><i class="fa fa-phone ico"></i></span>
                                        <input type="text" pattern="^\(?(\d{3})\)?[\s-]?\d{3}[\s-]?\d{4}$" maxlength="12" class="form-control"
                                               name="phone" placeholder="phone" data-error="Sorry, you must enter a valid phone number"
                                               value="{{ old('phone') }}" required>
                                    </div>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors">
                                        @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                            <div class="form-group has-feedback">
                                    <label for="inputAddr" class="control-label">Address</label>
                                    <div class="input-group" id="inputAddr">
                                        <span class="input-group-addon"><i class="fa fa-home ico"></i></span>
                                        <input type="text" maxlength="100" class="form-control"
                                               name="address" placeholder="Address"
                                               value="{{ old('address') }}" required>
                                    </div>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors">
                                        @if ($errors->has('address'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="form-group col-sm-6 has-feedback">
                                        <label for="inputCity" class="control-label">City</label>

                                        <div class="input-group" id="inputCity">
                                            <span class="input-group-addon"><i class="fa fa-location-arrow ico"></i></span>
                                            <input  class="form-control"
                                                   name="city" placeholder="City"
                                                   value="{{ old('city') }}" required>
                                        </div>
                                        <div class="help-block with-errors" >
                                            @if ($errors->has('city'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6 has-feedback">
                                        <label for="inputCountry" class="control-label">Country</label>
                                        <div class="input-group" id="inputCountry">
                                            <span class="input-group-addon"><i class="fa fa-map ico"></i></span>
                                            <select class="form-control" name="country" data-error="Sorry, you have to select one." required>
                                                <option value="">Select</option>
                                                @foreach (getCountryList() as $value)
                                                    <option value="{{ $value }}" {{ old('country') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="help-block with-errors" >
                                            @if ($errors->has('country'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('country') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-content bordered bordered-all primary margin-bottom-20 ">
                            <h4 class="box-title">Gaurdian Contact Information</h4>
                            <!-- /.box-title -->
                            <div class="form-group has-feedback">
                                <label for="inputGname" class="control-label">Name</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user ico"></i></span>
                                    <input type="text" pattern="(\w*|\s*|-)*" maxlength="15" class="form-control"
                                           id="inputGname" name="gname" placeholder="Full Name"
                                           value="{{ old('gname') }}" required>
                                </div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors">
                                    @if ($errors->has('gname'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('gname') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="inputGEmail" class="control-label">Email</label>
                                <div class="input-group">
                                    <span class="input-group-addon">@</span>
                                    <input type="email" class="form-control" id="inputGEmail" placeholder="Email"
                                           name="gemail" data-error="Bruh, that email address is invalid"
                                           value="{{ old('gemail') }}" required>
                                </div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors">
                                    @if ($errors->has('gemail'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('gemail') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                    <label for="inputGPhone" class="control-label">Phone</label>
                                    <div class="input-group" id="inputGPhone">
                                        <span class="input-group-addon"><i class="fa fa-phone ico"></i></span>
                                        <input type="text" pattern="^\(?(\d{3})\)?[\s-]?\d{3}[\s-]?\d{4}$" maxlength="15" class="form-control"
                                               name="gphone" placeholder="phone" data-error="Sorry, you must enter a valid phone number"
                                               value="{{ old('gphone') }}" required>
                                    </div>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors">
                                        @if ($errors->has('gphone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('gphone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                            <div class="form-group has-feedback">
                                    <label for="inputGAddr" class="control-label">Address</label>
                                    <div class="input-group" id="inputGAddr">
                                        <span class="input-group-addon"><i class="fa fa-home ico"></i></span>
                                        <input type="text"  maxlength="100" class="form-control"
                                               name="gaddress" placeholder="Address" value="{{ old('gaddress') }}" required>
                                    </div>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors">
                                        @if ($errors->has('gaddress'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('gaddress') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="form-group col-sm-6 has-feedback">
                                        <label for="inputGCity" class="control-label">City</label>

                                        <div class="input-group" id="inputGCity">
                                            <span class="input-group-addon"><i class="fa fa-location-arrow ico"></i></span>
                                            <input class="form-control"
                                                   name="gcity" placeholder="City"
                                                   value="{{ old('gcity') }}" required>
                                        </div>
                                        <div class="help-block with-errors" >
                                            @if ($errors->has('gcity'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('gcity') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6 has-feedback">
                                        <label for="inputGCountry" class="control-label">Country</label>
                                        <div class="input-group datepicker" id="inputGCountry">
                                            <span class="input-group-addon"><i class="fa fa-map ico"></i></span>
                                            <select class="form-control" name="gcountry" data-error="Sorry, you have to select one." required>
                                                <option value="">Select</option>
                                                @foreach (getCountryList() as $value)
                                                    <option value="{{ $value }}" {{ old('gcountry') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="help-block with-errors" >
                                            @if ($errors->has('gcountry'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('gcountry') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="form-group col-sm-6 has-feedback">
                                        <label for="inputRelation" class="control-label">Relation</label>

                                        <div class="input-group" id="inputRelation">
                                            <span class="input-group-addon"><i class="fa fa-users ico"></i></span>
                                            <input class="form-control"
                                                   name="relation" placeholder="eg. Aunt, Uncle, Mother, etc"
                                                   value="{{ old('relation') }}" required>
                                        </div>
                                        <div class="help-block with-errors" >
                                            @if ($errors->has('relation'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('relation') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6 has-feedback">
                                        <label for="inputGEmp" class="control-label">Employment Status</label>
                                        <div class="input-group" id="inputGEmp">
                                            <span class="input-group-addon"><i class="fa fa-money ico"></i></span>
                                            <select class="form-control" name="gemp" data-error="Sorry, you have to select one." required>
                                                <option value="">Select</option>
                                                @foreach (\App\Applications::empStatus() as $value)
                                                    <option value="{{ $value }}" {{ old('gemp') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="help-block with-errors" >
                                            @if ($errors->has('gemp'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('gemp') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-content bordered bordered-all primary margin-bottom-20 ">
                            <h4 class="box-title">Educational Information</h4>
                            <div class="form-group has-feedback">
                                <label for="inputInst" class="control-label">Institution</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-building ico"></i></span>
                                    <input type="text" pattern="^[_A-z]{1,}(\w*|\s*|\d*)*" class="form-control"
                                           id="inputInst" name="institution" placeholder="The name of your current educational institution" data-error="please enter a valid institution name"
                                           value="{{ old('institution') }}" required>
                                </div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="form-group col-sm-6 has-feedback">
                                        <label for="inputDOB" class="control-label">Programme</label>

                                        <div class="input-group" id="inputprogramme">
                                            <span class="input-group-addon"><i class="fa fa-graduation-cap ico"></i></span>
                                            <input type="text" class="form-control"
                                                   name="programme" placeholder="programme or course offering"
                                                   value="{{ old('programme') }}" required>
                                        </div>
                                        <div class="help-block with-errors" ></div>
                                    </div>
                                    <div class="form-group col-sm-6 has-feedback">
                                        <label for="inputPYear" class="control-label">Year</label>
                                        <div class="input-group" id="inputPYear">
                                            <span class="input-group-addon"><i class="fa fa-calendar-check-o ico"></i></span>
                                            <input type="number" max="8" min="1" class="form-control"
                                                   name="programme_year" placeholder="current level or year"
                                                   value="{{ old('programme_year') }}" required>
                                        </div>
                                        <div class="help-block with-errors" >
                                            @if ($errors->has('programme_year'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('programme_year') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <input type="checkbox" id="terms" name="isAgreed" data-error="Cannot submit without accepting" value="{{ old('isAgreed[0]') }}">
                                    <label for="terms">I agree to the MTN Foundation Scholarship terms and conditions</label>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
        </form>

    </div>
@endsection

@section('scripts')
    <!-- Dropify -->
    <script src="{{ asset('assets/plugin/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/fileUpload.demo.min.js') }}"></script>

    <!-- Datepicker -->
    <script src="{{ asset('assets/plugin/datepicker/js/bootstrap-datepicker.min.js') }}"></script>

    <!-- Validator -->
    <script src="{{ asset('assets/plugin/validator/validator.min.js') }}"></script>

    <!-- Demo Scripts -->
    <script src="{{ asset('assets/scripts/form.demo.js') }}"></script>
@endsection
