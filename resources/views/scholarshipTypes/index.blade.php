@extends('layouts.admin.main')

@section('page_title')
    Scholarships
@endsection

@section('content')
    <div class="row small-spacing">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box-content card" style="padding: 1%">
                        <form action="#" class="">
                            <div class="row form-horizontal">
                                <div class="col-lg-2 col-md-2 col-sm-5 " >
                                    <a  href="{{ route('types.create') }}" class="btn-icon btn-icon-left btn btn-info waves-effect waves-light">
                                        <i class="ico fa fa-plus"></i>Add New Scholarship</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row small-spacing">

        @if(isset($scholarships))
            @foreach($scholarships as $scholarship)
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="box-content card">
                        <h4 class="box-title {{ $scholarship->deleted_at == null ? "bg-primary":"bg-danger"}}"><i class="ico fa fa-space-shuttle"></i>{{$scholarship->name}}</h4>
                        <div class="dropdown js__drop_down">
                            <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                            <ul class="sub-menu">
                                <form method="post" action="{{ route('types.destroy', $scholarship->id) }}">
                                    @method('delete')
                                    @csrf
                                    <li><button><a href="{{route('types.edit',$scholarship->id)}}">Edit</a></button></li>

                                    <li><button type="submit" ><a>{{ $scholarship->deleted_at==null? "Delete" : "Enable" }}</a></button></li>
                                </form>

                            </ul>
                        </div>
                        <div class="card-content">
                            <h4>Description:</h4>
                            <p>{{$scholarship->description}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
        //
        @endif
    </div>
@endsection
