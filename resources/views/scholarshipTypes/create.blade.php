@extends('layouts.admin.main')

@section('page_title')
    Add New Scholarship Type
@endsection

@section('custom_css')

{{-- Dropify --}}
    <link rel="stylesheet" href="{{ asset('assets/plugin/dropify/css/dropify.min.css') }}">
@endsection

@section('content')
    <div class="row small-spacing">
        <form data-toggle="validator" method="post" action="{{ route('types.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="col-md-7 col-xs-12 ">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-content bordered bordered-all primary margin-bottom-20 ">
                            <h4 class="box-title">New Scholarship Programme</h4>
                                {{--Scholarship Name--}}
                                <div class="form-group has-feedback">
                                    <label for="inputName" class="control-label">Scholarship Name</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-ticket ico"></i></span>
                                        <input type="text" pattern="^[_A-z]{1,}$" maxlength="15" class="form-control"
                                               id="inputName" name="name" placeholder="Name of New scholarship Programme" data-error="Sorry, you can't leave this blank"
                                               value="{{ old('name') }}" required>
                                    </div>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors"></div>
                                </div>
                                {{--Scholarship Description--}}
                                <div class="form-group has-feedback">
                                    <label for="inputDescription" class="control-label">Description</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-edit ico"></i></span>
                                        <textarea type="text" class="form-control"
                                               id="inputDescription" name="description" placeholder=",
                                               Tell us about the new scholarship programme" data-error="Sorry, you can't leave this blank."
                                                  required>{{ old('description') }}</textarea>
                                    </div>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox" id="terms" name="isActive" data-error="Leaving account disabled? User can't login" value="{{old('isActive',"active")}}">
                                        <label for="terms">Activate Programme</label>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection

@section('scripts')
{{-- Dropify --}}
    <script src="{{ asset('assets/plugin/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/fileUpload.demo.min.js') }}"></script>

{{-- Validator --}}
    <script src="{{ asset('assets/plugin/validator/validator.min.js') }}"></script>
@endsection
