
@extends('layouts.admin.main')

@section('page_title')
    Disbursments
@endsection

@section('content')
    <div class="row small-spacing">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box-content card" style="padding: 1%">

                        <form action="#" class="">
                            <div class="row form-horizontal">
                                <div class="col-md-5 col-sm-5 ">
                                    <label for="date-range">Date Range</label>
                                    <div class="input-daterange input-group" id="date-range">
                                                <span class="input-group-addon bg-primary text-white"><i
                                                        class="fa fa-calendar"></i></span>
                                        <input id="start_date" type="text" class="form-control" name="start"/>
                                        <span class="input-group-addon bg-primary text-white">to</span>
                                        <input type="text" class="form-control" name="end"/>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <label for="sch-type">Transaction Status</label>
                                    <select id="sch-type" class="form-control" name="type">
                                        <option value="0">All</option>
                                        <option value="1">Completed</option>
                                        <option value="2">Processing</option>
                                        <option value="3">Cancelled</option>
                                    </select>
                                </div>
                                <div class="col-md-3 col-sm-1 " >
                                    <p> <br/></p>
                                    <button type="button"  class="btn btn-success waves-effect waves-light">Go</button>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- /.box-content card -->
                </div>
            </div>
        </div>
        <!-- /.col-xs-12 -->
    </div>

    <div class="row small-spacing">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box-content card">

                    </div>
                    <!-- /.box-content card -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box-content card">
                        <div class="table-responsive table-purchases">
                            <table class="table table-striped margin-bottom-10">
                                <thead>
                                <tr>
                                    <th style="width:30%;">Transaction ID</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>State</th>
                                    <th style="width:5%;"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>DTID-6582366</td>
                                    <td>$71</td>
                                    <td>Nov 12,2019</td>
                                    <td class="text-success">Completed</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582765</td>
                                    <td>$142</td>
                                    <td>Nov 10,2019</td>
                                    <td class="text-danger">Cancelled</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582369</td>
                                    <td>$200</td>
                                    <td>Nov 01,2019</td>
                                    <td class="text-warning">Pending</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582323</td>
                                    <td>$200</td>
                                    <td>Oct 28,2019</td>
                                    <td class="text-warning">Pending</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582363</td>
                                    <td>$200</td>
                                    <td>Oct 28,2019</td>
                                    <td class="text-success">Completed</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582234</td>
                                    <td>$71</td>
                                    <td>Oct 22,2019</td>
                                    <td class="text-success">Completed</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>

                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-content card -->
                </div>
            </div>
        </div>
        <!-- /.col-xs-12 -->
    </div>
@endsection
