
@extends('layouts.admin.main')

@section('page_title')
    Profile
@endsection

@section('content')
    <div class="row small-spacing">
        <div class="col-md-3 col-xs-12">
            <div class="box-content bordered primary margin-bottom-20">
                <div class="profile-avatar">
                    <img src="http://placehold.it/450x450" alt="">
                    <a href="#" class="btn btn-block btn-inbox"><i class="fa fa-envelope"></i> Send Message</a>
                    <h3><strong>Betty Simmons</strong></h3>
                    <h4>Teacher at Our Company, Inc.</h4>
                    <p>Ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
                        dolores eos qui ratione voluptatem sequi nesciunt.</p>
                </div>
                <!-- .profile-avatar -->
                <table class="table table-hover no-margin">
                    <tbody>
                    <tr>
                        <td>Status</td>
                        <td><span class="notice notice-danger">Active</span></td>
                    </tr>
                    <tr>
                        <td>User Rating</td>
                        <td><i class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i
                                class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i
                                class="fa fa-star text-warning"></i></td>
                    </tr>
                    <tr>
                        <td>Member Since</td>
                        <td>Jan 07, 2014</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-content bordered -->

            <div class="box-content">
                <h4 class="box-title"><i class="ico fa fa-files"></i>Uploaded Documents</h4>
                <ul class="profile-friends-list list-inline">
                    <li><a href="#" class="avatar"><img src="http://placehold.it/128x128" alt="">lol</a></li>
                    <li><a href="#" class="avatar"><img src="http://placehold.it/128x128" alt=""></a></li>
                    <li><a href="#" class="avatar"><img src="http://placehold.it/128x128" alt=""></a></li>
                    <li><a href="#" class="avatar"><img src="http://placehold.it/128x128" alt=""></a></li>
                    <li><a href="#" class="avatar"><img src="http://placehold.it/128x128" alt=""></a></li>
                </ul>
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-md-3 col-xs-12 -->
        <div class="col-md-9 col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-content card">
                        <h4 class="box-title"><i class="fa fa-user ico"></i>About</h4>
                        <!-- /.box-title -->
                        <div class="dropdown js__drop_down">
                            <a href="#"
                               class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                            <ul class="sub-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else there</a></li>
                                <li class="split"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                            <!-- /.sub-menu -->
                        </div>
                        <!-- /.dropdown js__dropdown -->
                        <div class="card-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-xs-5"><label>First Name:</label></div>
                                        <!-- /.col-xs-5 -->
                                        <div class="col-xs-7">Betty</div>
                                        <!-- /.col-xs-7 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-xs-5"><label>Last Name:</label></div>
                                        <!-- /.col-xs-5 -->
                                        <div class="col-xs-7">Simmons</div>
                                        <!-- /.col-xs-7 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-xs-5"><label>User Name:</label></div>
                                        <!-- /.col-xs-5 -->
                                        <div class="col-xs-7">Betty</div>
                                        <!-- /.col-xs-7 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-xs-5"><label>Email:</label></div>
                                        <!-- /.col-xs-5 -->
                                        <div class="col-xs-7">youremail@gmail.com</div>
                                        <!-- /.col-xs-7 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-xs-5"><label>City:</label></div>
                                        <!-- /.col-xs-5 -->
                                        <div class="col-xs-7">Los Angeles</div>
                                        <!-- /.col-xs-7 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-xs-5"><label>Country:</label></div>
                                        <!-- /.col-xs-5 -->
                                        <div class="col-xs-7">United States</div>
                                        <!-- /.col-xs-7 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-xs-5"><label>Birthday:</label></div>
                                        <!-- /.col-xs-5 -->
                                        <div class="col-xs-7">Jan 22, 1984</div>
                                        <!-- /.col-xs-7 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-xs-5"><label>Interests:</label></div>
                                        <!-- /.col-xs-5 -->
                                        <div class="col-xs-7">Basketball, Web, Design, etc.</div>
                                        <!-- /.col-xs-7 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-xs-5"><label>Website:</label></div>
                                        <!-- /.col-xs-5 -->
                                        <div class="col-xs-7"><a href="#">yourwebsite.com</a></div>
                                        <!-- /.col-xs-7 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-xs-5"><label>Phone:</label></div>
                                        <!-- /.col-xs-5 -->
                                        <div class="col-xs-7">+1-234-5678</div>
                                        <!-- /.col-xs-7 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.col-md-6 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-content -->
                    </div>
                    <!-- /.box-content card -->
                </div>
                <!-- /.col-md-12 -->
                <div class="col-md-6 col-xs-12">
                    <div class="box-content card">
                        <h4 class="box-title"><i class="fa fa-file-text ico"></i> Experience</h4>
                        <!-- /.box-title -->
                        <div class="dropdown js__drop_down">
                            <a href="#"
                               class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                            <ul class="sub-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else there</a></li>
                                <li class="split"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                            <!-- /.sub-menu -->
                        </div>
                        <!-- /.dropdown js__dropdown -->
                        <div class="card-content">
                            <ul class="dot-list">
                                <li><a href="#">Owner</a> at <a href="#">MTN Foundation </a>.<span class="date">March 2013 ~ Now</span>
                                </li>
                                <li><a href="#">CEO</a> at <a href="#">CEO Company</a>.<span class="date"> March 2011 ~ February 2013</span>
                                </li>
                                <li><a href="#">Web Designer</a> at <a href="#">Web Design Company Ltd.</a>.<span
                                        class="date"> March 2010 ~ February 2011</span></li>
                                <li><a href="#">Sales</a> at <a href="#">Sales Company Ltd.</a>.<span class="date"> March 2009 ~ February 2010</span>
                                </li>
                            </ul>
                        </div>
                        <!-- /.card-content -->
                    </div>
                    <!-- /.box-content card -->
                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6 col-xs-12">
                    <div class="box-content card">
                        <h4 class="box-title"><i class="fa fa-trophy ico"></i> Education</h4>
                        <!-- /.box-title -->
                        <div class="dropdown js__drop_down">
                            <a href="#"
                               class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                            <ul class="sub-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else there</a></li>
                                <li class="split"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                            <!-- /.sub-menu -->
                        </div>
                        <!-- /.dropdown js__dropdown -->
                        <div class="card-content">
                            <ul class="dot-list">
                                <li><a href="#">Students</a> at <a href="#">CEO Education</a>.<span class="date">March 2013 ~ Now</span>
                                </li>
                                <li><a href="#">Students</a> at <a href="#">Web Design Education</a>.<span
                                        class="date">March 2011 ~ February 2013</span></li>
                                <li><a href="#">Students</a> at <a href="#">Sales School</a>.<span class="date"> March 2010 ~ February 2011</span>
                                </li>
                                <li><a href="#">Students</a> at <a href="#">High School</a>.<span class="date"> March 2009 ~ February 2010</span>
                                </li>
                            </ul>
                        </div>
                        <!-- /.card-content -->
                    </div>
                    <!-- /.box-content card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box-content card">
                        <h4 class="box-title">Disbursment Profile Summary</h4>
                        <!-- /.box-title -->
                        <div class="dropdown js__drop_down">
                            <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                            <ul class="sub-menu">
                                <li><a href="#">Product</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else there</a></li>
                                <li class="split"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                            <!-- /.sub-menu -->
                        </div>
                        <!-- /.dropdown js__dropdown -->
                        <div class="table-responsive table-purchases">
                            <table class="table table-striped margin-bottom-10">
                                <thead>
                                <tr>
                                    <th style="width:30%;">Transaction ID</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>State</th>
                                    <th style="width:5%;"></th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <div class="text-center margin-top-20"><a href="{{route('disbursement-profile')}}" class="btn btn-default">See All Disbursments <i class="fa fa-angle-double-right"></i></a></div>
                                </tr>
                                </tfoot>

                                <tbody>
                                <tr>
                                    <td>DTID-6582366</td>
                                    <td>$71</td>
                                    <td>Nov 12,2019</td>
                                    <td class="text-success">Completed</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582765</td>
                                    <td>$142</td>
                                    <td>Nov 10,2019</td>
                                    <td class="text-danger">Cancelled</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582369</td>
                                    <td>$200</td>
                                    <td>Nov 01,2019</td>
                                    <td class="text-warning">Pending</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582323</td>
                                    <td>$200</td>
                                    <td>Oct 28,2019</td>
                                    <td class="text-warning">Pending</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582363</td>
                                    <td>$200</td>
                                    <td>Oct 28,2019</td>
                                    <td class="text-success">Completed</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582234</td>
                                    <td>$71</td>
                                    <td>Oct 22,2019</td>
                                    <td class="text-success">Completed</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>

                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-content card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.col-md-9 col-xs-12 -->
    </div>
@endsection
