
@extends('layouts.admin.main')

@section('page_title')
    Disbursments
@endsection

@section('content')
    <div class="row small-spacing">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box-content card" style="padding: 1%">

                        <form action="#" class="">
                            <div class="row form-horizontal">
                                <div class="col-md-5 col-sm-5 ">
                                    <label for="date-range">Date Range</label>
                                    <div class="input-daterange input-group" id="date-range">
                                                <span class="input-group-addon bg-primary text-white"><i
                                                        class="fa fa-calendar"></i></span>
                                        <input id="start_date" type="text" class="form-control" name="start"/>
                                        <span class="input-group-addon bg-primary text-white">to</span>
                                        <input type="text" class="form-control" name="end"/>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <label for="sch-type">Scholarship Type</label>
                                    <select id="sch-type" class="form-control" name="type">
                                        <option value="0">All selected</option>
                                        <option value="1">Legacy Scholarship </option>
                                        <option value="2">10 Million Scholarship</option>
                                        <option value="3">TIAP Scholarship</option>
                                    </select>
                                </div>
                                <div class="col-md-3 col-sm-1 " >
                                    <p> <br/></p>
                                    <button type="button"  class="btn btn-success waves-effect waves-light">Go</button>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- /.box-content card -->
                </div>
            </div>
        </div>
        <!-- /.col-xs-12 -->
    </div>

    <div class="row small-spacing">
        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content bg-success text-white">
                <div class="statistics-box with-icon">
                    <i class="ico small fa fa-usd"></i>
                    <p class="text text-white">PAID</p>
                    <h2 class="counter">1,256,525 </h2>
                </div>
                <p class="text margin-bottom-0" style="text-align: right;">756,525 Transactions</p>
            </div>
            <!-- /.box-content -->
        </div>

        <!-- /.col-lg-3 col-md-6 col-xs-12 -->
        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content bg-warning text-white">
                <div class="statistics-box with-icon">
                    <i class="ico small fa fa-usd"></i>
                    <p class="text text-white">PENDING</p>
                    <h2 class="counter">2,637</h2>
                </div>
                <p class="text margin-bottom-0" style="text-align: right;">756,525 Transactions</p>
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-lg-3 col-md-6 col-xs-12 -->
        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content bg-info text-white">
                <div class="statistics-box with-icon">
                    <i class="ico small fa fa-users"></i>
                    <p class="text text-white">BENEFICIARIES </p>
                    <h2 class="counter">6,382</h2>
                </div>
                <p class="text margin-bottom-0" style="text-align: right;">756,525 Transactions</p>
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-lg-3 col-md-6 col-xs-12 -->
        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content bg-danger text-white">
                <div class="statistics-box with-icon">
                    <i class="ico small fa fa-usd"></i>
                    <p class="text text-white">FAILED</p>
                    <h2 class="counter">12,564</h2>
                </div>
                <p class="text margin-bottom-0" style="text-align: right;">756,525 Transactions</p>
            </div>
            <!-- /.box-content -->
        </div>

        <!-- /.col-lg-3 col-md-6 col-xs-12 -->
    </div>
    <!-- .row -->
    <div class="row small-spacing">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box-content card">
                        <div class="table-responsive table-purchases">
                            <table class="table table-striped margin-bottom-10">
                                <thead>
                                <tr>
                                    <th style="width:30%;">Transaction ID</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>State</th>
                                    <th style="width:5%;"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>DTID-6582366</td>
                                    <td>$71</td>
                                    <td>Nov 12,2019</td>
                                    <td class="text-success">Completed</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582765</td>
                                    <td>$142</td>
                                    <td>Nov 10,2019</td>
                                    <td class="text-danger">Cancelled</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582369</td>
                                    <td>$200</td>
                                    <td>Nov 01,2019</td>
                                    <td class="text-warning">Pending</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582323</td>
                                    <td>$200</td>
                                    <td>Oct 28,2019</td>
                                    <td class="text-warning">Pending</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582363</td>
                                    <td>$200</td>
                                    <td>Oct 28,2019</td>
                                    <td class="text-success">Completed</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>
                                <tr>
                                    <td>DTID-6582234</td>
                                    <td>$71</td>
                                    <td>Oct 22,2019</td>
                                    <td class="text-success">Completed</td>
                                    <td><a href="#"><i class="fa fa-plus-circle"></i></a></td>
                                </tr>

                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-content card -->
                </div>
            </div>
        </div>
        <!-- /.col-xs-12 -->
    </div>
@endsection
