@extends('auth.layout')

@section('page_title')
    Login
@endsection

@section('content')
    <main>
        <div id="primary" class="p-t-b-100 height-full bg-yellow">
            <div class="container bg-yellow">
                <div class="row">
                    <div class="col-lg-4 mx-md-auto paper-card">
                        <div class="text-center">
                            <!--replace with company logo -->
                            <img src="assets/img/dummy/u4.png" alt="">
                            <h3 class="mt-2">Welcome</h3>
                        <a  href="{{route('apply')}}" class="btn btn-primary btn-lg btn-block bg-yellow bolder">Apply For Scholarship</a>
                            <p class="p-t-b-20">Kindly provide your email and password</p>
                        </div>
                        <form method="post" action="{{ route('login') }}">
                            @csrf


                            <div class="form-group has-icon"><i class="icon icon-user"></i>
                                <input type="email" class="form-control form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group has-icon"><i class="icon-lock"></i>
                                <input type="password" class="form-control form-control-lg {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required
                                       placeholder="Password" >

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary btn-lg btn-block bg-yellow bolder" value="Login">

                            <p class="forget-pass ">
                                <a href="{{ route('password.request') }}">Forgot your password?</a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
