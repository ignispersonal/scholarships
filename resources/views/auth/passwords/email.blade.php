@extends('auth.layout')

@section('page_title')
    Reset Password
@endsection

@section('content')
    <main>
        <div id="primary" class="p-t-b-100 height-full bg-yellow">
            <div class="container bg-yellow">
                <div class="row">
                    <div class="col-lg-5 mx-md-auto paper-card">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="text-center">
                            <h2 class="mt-2">Reset Your Password</h2>
                            <p class="p-t-b-20">You can reset your password by providing your email address</p>
                        </div>
                        <form action="{{ route('password.email') }}" method="post">
                            @csrf
                            <div class="form-group has-icon"><i class="icon icon-mail"></i>
                                <input id="email" type="email" class="form-control form-control-lg
                                {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"
                                       placeholder="email" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="row forget-pass">
                                <button type="submit" class="btn btn-primary green m-auto col-5">
                                    {{ __('Send Reset Link') }}
                                </button><p class="text-black s-18 pt-2">or</p>
                                <a class=" btn btn-primary red m-auto col-5" href="{{ route('login') }}">Return To Login</a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
