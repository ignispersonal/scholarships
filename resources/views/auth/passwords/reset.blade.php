@extends('auth.layout')

@section('content')
    <main>
        <div id="primary" class="p-t-b-100 height-full bg-yellow">
            <div class="container bg-yellow">
                <div class="row">
                    <div class="col-lg-5 mx-md-auto paper-card">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="text-center">
                            <h2 class="mt-2">Reset Your Password</h2>
                            <p class="p-t-b-20">You can reset your password by providing your email address</p>
                        </div>
                        <form method="POST" action="{{ route('password.update') }}">
                            @csrf

                            {{--                        <input type="hidden" name="token" value="{{ $token }}">--}}

                            <div class="form-group row has-icon"><i class="icon icon-mail"></i>
                                <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" placeholder="email"  required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group row has-icon"><i class="icon icon-lock"></i>
                                <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" placeholder="password" required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group row has-icon"><i class="icon icon-lock"></i>
                                <input id="password-confirm" type="password" class="form-control-lg form-control" placeholder="confirm password" name="password_confirmation" required autocomplete="new-password">
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary bg-yellow">
                                        {{ __('Reset Password') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
