@extends('layouts.admin.main')

@section('page_title')
    User Management
@endsection

@section('content')
    <div class="row small-spacing">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box-content card" style="padding: 1%">
                        <form action="#" class="">
                            <div class="row form-horizontal">
                                <div class="col-lg-5 col-md-6 col-sm-12">
                                    <label for="user-type">User Type</label>
                                    <select id="user-type" class="form-control" name="type">
                                        <option value="0">All selected</option>
                                        <option value="1">Legacy Scholarship </option>
                                        <option value="2">10 Million Scholarship</option>
                                        <option value="3">TIAP Scholarship</option>
                                    </select>
                                </div>
                                <div class="col-lg-1 col-md-2 col-sm-3 " >
                                    <p> <br/></p>
                                    <button type="button"  class="btn btn-success waves-effect waves-light">Go</button>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-5 " >
                                    <p> <br/></p>
                                    <a  href="#" class="btn-icon btn-icon-left btn btn-info waves-effect waves-light"><i class="ico fa fa-user-plus"></i>Add User</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-content card -->
                </div>
            </div>
        </div>
        <!-- /.col-xs-12 -->
    </div>
    <div class="row small-spacing">

        @if(isset($users))
            @foreach($users as $user)
                <div class="col-lg-3 col-md-6 col-xs-12">
                    <div class="box-content user-info">
                        <div class="avatar"><img src="{{ old('photo', asset('storage/'.$user['photo'])) }}" alt=""></div>
                        <!-- /.avatar -->
                        <div class="right-content">
                            <h4 class="name">{{$user->fname.' '.$user->lname}}</h4>
                            <!-- /.name -->
                            <p><a href="mailto:{{$user->email}}"></a>{{ $user->email }}</p>
                            <div class="text-warning small"></div>
                            <!-- /.text-warning -->
                        </div>
                        <div class="modal-footer" style="margin-top: 10%;padding-bottom: unset;text-align: left;">
                            <a href="{{route('users.edit',$user->id)}}" class="btn-icon btn-icon-left btn btn-primary btn-sm waves-effect waves-light text-white">
                                <i class="ico fa fa-pencil"></i>edit
                            </a>
                            <a href="#" class="btn-icon btn-icon-left btn btn-danger btn-sm waves-effect waves-light text-white" data-dismiss="modal">
                                <i class="ico fa fa-ban"></i>disable
                            </a>
                        </div>
                        <!-- /.right-content -->
                    </div>
            <!-- /.user-info -->
                </div>
            @endforeach
        @else
        //
        @endif
    </div>
@endsection
