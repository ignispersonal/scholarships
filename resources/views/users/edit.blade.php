@extends('layouts.admin.main')

@section('page_title')
    Edit User
@endsection

@section('custom_css')

    <!-- Dropify -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/dropify/css/dropify.min.css') }}">
@endsection

@section('content')
    <div class="row small-spacing">
        <form data-toggle="validator" method="POST" action="{{ route('users.update',$user->id) }}" enctype="multipart/form-data">
            @csrf
            {{ method_field('PATCH') }}
            <div class="col-md-3 col-xs-12">
                <div class="box-content bordered bordered-all primary margin-bottom-20">
                    <div class="profile-avatar">
                        <h4 class="box-title">Photo</h4>

                    </div>
                    <!-- /.box-content -->
                    <div class="profile-avatar has-feedback">
                        <input type="file" id="input-file-to-destroy" class="dropify" name="photo" data-allowed-formats="portrait square"
                               data-default-file="{{ old('photo', asset('storage/'.$user['photo'])) }}" data-max-file-size="1M" data-max-height="450" data-error="Sorry, you can't leave this blank" />
                        <p class="help margin-top-10">Only portrait or square images, 1M max and 450 max-height.</p>
                    </div>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                    <!-- .profile-avatar -->
                </div>
                <!-- /.box-content bordered -->
            </div>

            <div class="col-md-7 col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-content bordered bordered-all primary margin-bottom-20 ">
                            <h4 class="box-title">Create User</h4>
                            <!-- /.box-title -->

                            <div class="form-group has-feedback">
                                <label for="inputFname" class="control-label">First Name</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user ico"></i></span>
                                    <input type="text" pattern="^[_A-z]{1,}$" maxlength="15" class="form-control"
                                           id="inputFname" name="fname" placeholder="First Name" data-error="Sorry, you can't leave this blank"
                                           value="{{ old('fname',$user->fname )}}" required>
                                </div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="inputLname" class="control-label">Last Name</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user ico"></i></span>
                                    <input type="text" pattern="^[_A-z]{1,}$" maxlength="15" class="form-control"
                                           id="inputLname" name="lname" placeholder="Last Name" data-error="Sorry, you can't leave this blank"
                                           value="{{ old('lname',$user->lname) }}" required>
                                </div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="inputEmail" class="control-label">Email</label>
                                <div class="input-group">
                                    <span class="input-group-addon">@</span>
                                    <input type="email" class="form-control" id="inputEmail" placeholder="Email"
                                           name="email" data-error="Bruh, that email address is invalid"
                                           value="{{ old('email',$user->email)  }}" required>
                                </div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="inputPassword" class="control-label">Password</label>

                                        <input type="password" data-minlength="6" class="form-control" id="inputPassword"
                                               name="password" placeholder="Password"
                                               value="" >
                                        <div class="help-block">Minimum of 6 characters</div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="inputPasswordConfirm" class="control-label">Confirm Password</label>

                                        <input type="password" class="form-control" id="inputPasswordConfirm"
                                               data-match="#inputPassword" data-match-error="Whoops, these don't match"
                                               name="password_confirmation" placeholder="Confirm" value="">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="user-role">User Role</label>
                                <select id="user-role" class="form-control" name="role" >
                                    <option value="">Select Role</option>
                                    @foreach ($roles as $key => $value)
                                        <option value="{{ $key }}" {{ old('role', isset($user->roles[0]->id)? $user->roles[0]->id : "" ) == $key ? 'selected' : ''}}>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <input type="checkbox" id="terms" name="isActive" value="true" data-error="Leaving account disabled? User can't login"
                                    @if(old('isActive',!isset($user->deleted_at))) checked @endif>
                                    <label for="terms">Activate account</label>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                            </div>

                        </div>
                        <!-- /.box-content card -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
        </form>

    </div>
@endsection

@section('scripts')
    <!-- Dropify -->
    <script src="{{ asset('assets/plugin/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/fileUpload.demo.min.js') }}"></script>

    <!-- Validator -->
    <script src="{{ asset('assets/plugin/validator/validator.min.js') }}"></script>
@endsection
