@extends('layouts.admin.main')

@section('page_title')
    Payment Requests
@endsection

@section('content')

    <div class="row small-spacing">
        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content bg-success text-white">
                <div class="statistics-box with-icon">
                    <i class="ico small fa fa-usd"></i>
                    <p class="text text-white">PAID</p>
                    <h2 class="counter">1,256,525 </h2>
                </div>
                <p class="text margin-bottom-0" style="text-align: right;">756,525 Transactions</p>
            </div>
            <!-- /.box-content -->
        </div>

        <!-- /.col-lg-3 col-md-6 col-xs-12 -->
        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content bg-warning text-white">
                <div class="statistics-box with-icon">
                    <i class="ico small fa fa-usd"></i>
                    <p class="text text-white">PENDING</p>
                    <h2 class="counter">2,637</h2>
                </div>
                <p class="text margin-bottom-0" style="text-align: right;">756,525 Transactions</p>
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-lg-3 col-md-6 col-xs-12 -->
        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content bg-info text-white">
                <div class="statistics-box with-icon">
                    <i class="ico small fa fa-users"></i>
                    <p class="text text-white">BENEFICIARIES </p>
                    <h2 class="counter">6,382</h2>
                </div>
                <p class="text margin-bottom-0" style="text-align: right;">756,525 Transactions</p>
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-lg-3 col-md-6 col-xs-12 -->
        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content bg-danger text-white">
                <div class="statistics-box with-icon">
                    <i class="ico small fa fa-usd"></i>
                    <p class="text text-white">FAILED</p>
                    <h2 class="counter">12,564</h2>
                </div>
                <p class="text margin-bottom-0" style="text-align: right;">756,525 Transactions</p>
            </div>
            <!-- /.box-content -->
        </div>

        <!-- /.col-lg-3 col-md-6 col-xs-12 -->
    </div>
    <!-- .row -->
    <div class="row small-spacing">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box-content card">
                        <div class="table-responsive table-purchases">
                            <table class="table table-striped margin-bottom-10">
                                <thead>
                                <tr>
                                    <th style="width:30%;">Transaction ID</th>
                                    <th>Vendor/ Institution</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>State</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                {{--
                                    "id" => "752024bb-4d1d-43f1-a8e1-d0003c6a5556"
                                    "title" => "A test request"
                                    "description" => "A test request description"
                                    "amount" => 2000.0
                                    "vendor_institution" => "AITI-KACE"
                                    "date_due" => "2019-07-31"
                                    "user_id" => "63cd2d39-4523-49cb-9744-a9c4cf286dc4"
                                    "attached_document" => "payment_requests//JxNRllvSCKXov5pITHTYQh6SKFLSUcrKlC1EdbXw.png"
                                    "status" => "pending"
                                    "deleted_at" => null
                                    "created_at" => "2019-07-25 11:38:33"
                                    "updated_at" => "2019-07-25 11:38:33"
                                    --}}
                                <tbody>
                                @foreach ($paymentRequests as $paymentRequest)
                                    <tr>
                                        <td>{{$paymentRequest->id}}</td>
                                        <td>{{$paymentRequest->vendor_institution}}</td>
                                        <td>GHC {{$paymentRequest->amount}}</td>
                                        <td>{{$paymentRequest->date_due}}</td>
                                        <td class="{{$paymentRequest->statusColor()}}">{{$paymentRequest->status}}</td>
                                        <td>
                                            <a href="{{route('paymentrequests.edit',$paymentRequest->id)}}"><i class="ico fa fa-pencil"></i></a>
                                            <a href="{{route('paymentrequests.generate',$paymentRequest->id)}}"><i class="ico fa fa-paper-plane"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-content card -->
                </div>
            </div>
        </div>
        <!-- /.col-xs-12 -->
    </div>
@endsection

