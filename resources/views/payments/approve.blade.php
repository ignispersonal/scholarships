@extends('layouts.admin.main')

@section('page_title')
   Payment Request Approval
@endsection

@section('content')
    <div class="row small-spacing">
        <form data-toggle="validator" method="post" action="" enctype="multipart/form-data">
            @csrf

            <div class="col-md-7 col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-content bordered bordered-all primary margin-bottom-20 ">
                            <h4 class="box-title">Approve Payment Request</h4>
                            <!-- /.box-title -->

                                <div class="form-group has-feedback">
                                    <label for="requestFile" class="control-label">Beneficiary Name</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" class="form-control" value="{{"Beneficiary name"}}"
                                               readonly required>
                                    </div>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors"></div>
                                </div>

                                {{-- vendor name --}}
                                <div class="form-group has-feedback">
                                        <label for="vendor" class="control-label">Vendor Name</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-building-o"></i></span>
                                        <input type="text" class="form-control" value="{{old('vendor')}}"  data-error="Sorry, you can't leave this blank"
                                               placeholder="Enter name of vendor to pay to"    required>
                                        </div>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <div class="help-block with-errors">
                                                @if ($errors->has('vendor'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('vendor') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                {{-- end vendor name --}}


                                 {{-- amount in figures --}}
                                 <div class="form-group has-feedback">
                                        <label for="amount_fig" class="control-label">Amount in figures</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-sort-numeric-asc"></i></span>
                                        <input type="number" class="form-control" value="{{old('amount_fig')}}"  data-error="Sorry, you can't leave this blank"
                                              name="amount_fig" placeholder="Enter name of vendor to pay to"    required>
                                        </div>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <div class="help-block with-errors">
                                                @if ($errors->has('amount_fig'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('amount_fig') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                {{-- end amount in figures --}}

                                 {{-- amount in words --}}
                                 <div class="form-group has-feedback">
                                        <label for="amount_w" class="control-label">Amount in words</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-building-o"></i></span>
                                        <input type="text" class="form-control" value="{{old('amount_w')}}"  data-error="Sorry, you can't leave this blank"
                                               placeholder="Enter amount in words"    required>
                                        </div>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <div class="help-block with-errors">
                                                @if ($errors->has('amount_w'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('amount_w') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                {{-- end amount in words --}}

{{-- description of payment --}}
                                <div class="form-group has-feedback">
                                    <label for="description" class="control-label">Description of payment request</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                        <textarea id="description" class="form-control" name="description" maxlength="225" rows="2" data-error="This field is required"
                                    placeholder="This will contain the description as entered by beneficiary">{{old('description') }}</textarea>
                                    </div>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors">
                                            @if ($errors->has('description'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
{{-- end of description of payment --}}

{{-- division selection --}}
                                <div class="form-group has-feedback">
                                        <label for="division" class="control-label">Select Division</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-building"></i></span>
                                        <select class="form-control" name="division" data-error="Sorry, you have to select one." required>
                                            <option value="">Select</option>
                                            {{-- loop through divisions --}}
                                            {{-- @foreach (\App\Applications::empStatus() as $value)
                                            <option value="{{ $value }}" {{ old('division') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                    <div class="help-block with-errors" >
                                        @if ($errors->has('division'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('division') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
{{-- end of division selection --}}

{{-- attached type --}}
                                <div class="form-group has-feedback">
                                        <label for="division" class="control-label">Select Attachment Type</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-file"></i></span>
                                        <select class="form-control" name="attached" data-error="Sorry, you have to select one." required>
                                            <option value="">Select </option>
                                            {{-- loop through divisions --}}
                                            {{-- @foreach (\App\Applications::empStatus() as $value)
                                            <option value="{{ $value }}" {{ old('attached') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                    <div class="help-block with-errors" >
                                        @if ($errors->has('attached'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('attached') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
{{-- end of attached selection --}}

{{-- expense type --}}
                            <div class="form-group has-feedback">
                                    <label for="division" class="control-label">Select Expense Type</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-file-text"></i></span>
                                    <select class="form-control" name="expense" data-error="Sorry, you have to select one." required>
                                        <option value="">Select </option>
                                        {{-- loop through divisions --}}
                                        {{-- @foreach (\App\Applications::empStatus() as $value)
                                        <option value="{{ $value }}" {{ old('expense') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                        @endforeach --}}
                                    </select>
                                </div>
                                <div class="help-block with-errors" >
                                    @if ($errors->has('expense'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('expense') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
{{-- end of expense selection --}}

{{-- payment type --}}
                            <div class="form-group has-feedback">
                                    <label for="division" class="control-label">Select Payment Type</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list-alt"></i></span>
                                    <select class="form-control" name="payment_type" data-error="Sorry, you have to select one." required>
                                        <option value="">Select </option>
                                        {{-- loop through divisions --}}
                                        {{-- @foreach (\App\Applications::empStatus() as $value)
                                        <option value="{{ $value }}" {{ old('payment_type') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                        @endforeach --}}
                                    </select>
                                </div>
                                <div class="help-block with-errors" >
                                    @if ($errors->has('payment_type'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('payment_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
{{-- end of payment selection --}}


{{-- payment type --}}
                            <div class="form-group has-feedback">
                                    <label for="division" class="control-label">Select Currency</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <select class="form-control" name="currency" data-error="Sorry, you have to select one." required>
                                        <option value="">Select </option>
                                        {{-- loop through divisions --}}
                                        {{-- @foreach (\App\Applications::empStatus() as $value)
                                        <option value="{{ $value }}" {{ old('currency') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                        @endforeach --}}
                                    </select>
                                </div>
                                <div class="help-block with-errors" >
                                    @if ($errors->has('currency'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('currency') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
{{-- end of payment selection --}}

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                </div>

                        </div>
                        <!-- /.box-content card -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
        </form>

    </div>
@endsection
