@extends('layouts.admin.main')

@section('page_title')
   Payment Request Approval
@endsection

@section('custom_css')
    <!-- X-Editable -->
    <link rel="stylesheet" href="assets/plugin/x-editable/bootstrap3-editable/css/bootstrap-editable.css">
@endsection

@section('content')
    <div class="row small-spacing" id="print">
        <form data-toggle="validator" method="post" action="" enctype="multipart/form-data" class="form-horizontal">
            @csrf

            <div class="col-md-12 col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-content bordered bordered-all primary margin-bottom-20 ">

                            <div class="form-group col-12 box-title">
                                    <h4 class=""> Payment Approval Request <img class="pull-right" src="{{ asset('assets/images/mtn.resized.png') }}" ></h4>

                            </div>
                            <br>
                            <div class="row left-content">
                                <div class="form-group col-sm-6 has-feedback">
                                    <label for="inputDivision" class="col-sm-3 control-label">Division:</label>
                                    <div class="input-group col-sm-9">
                                        <select id="inputDivision" class="form-control" name="division" data-error="Sorry, you have to select one." required>
                                            <option value="">Select</option>
                                            @foreach (getDivisionList() as $value)
                                                <option value="{{ $value }}" {{ old('division') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="help-block with-errors text-danger" >
                                        @if ($errors->has('division'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('division') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-sm-6 has-feedback">
                                    <label for="inputParSerial" class="col-sm-3 control-label">PAR Serial Number:</label>
                                    <div class="input-group col-sm-9">
                                        <input type="text" id="inputParSerial" class="form-control" name="par_serial_number" value="{{ old('par_serial_number') }}"
                                               required>
                                    </div>
                                    <div class="help-block with-errors text-danger" >
                                        @if ($errors->has('par_serial_number'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('par_serial_number') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row left-content">
                                <div class="form-group col-sm-6 has-feedback">
                                    <label for="inputAttached" class="col-sm-3 control-label">Attached:</label>
                                    <div class="input-group col-sm-9">
                                        <select id="inputAttached" class="form-control" name="attached" data-error="Sorry, you have to select one." required>
                                            <option value="">Select</option>
                                            @foreach (getAttachedList() as $value)
                                                <option value="{{ $value }}" {{ old('attached') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="help-block with-errors text-danger" >
                                        @if ($errors->has('attached'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('attached') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-sm-6 has-feedback">
                                    <label for="inputRef" class="col-sm-3 control-label">PO/Invoice ref:</label>
                                    <div class="input-group col-sm-9">
                                        <input type="text" id="inputRef" class="form-control" name="ref" value="{{ old('ref') }}"
                                               required>
                                    </div>
                                    <div class="help-block with-errors text-danger" >
                                        @if ($errors->has('ref'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('ref') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row box-title " style="border-bottom-style: inherit;">
                                <h4 class="" style="color: black;">Payment Details</h4>
                            </div>

                            <div class="row left-content">
                                <div class="form-group col-sm-6 has-feedback">
                                    <label for="inputVendor" class="col-sm-3 control-label">Vendor Name:</label>
                                    <div class="input-group col-sm-9">
                                        <input type="text" id="inputVendor" class="form-control" name="vendor" value="{{ old('vendor') }}"
                                               required>
                                    </div>
                                    <div class="help-block with-errors text-danger" >
                                        @if ($errors->has('vendor'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('vendor') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-sm-6 has-feedback">
                                    <label for="inputExpenditure" class="col-sm-3 control-label">Expenditure Type:</label>
                                    <div class="input-group col-sm-9">
                                        <select id="inputExpenditure" class="form-control" name="expenditure_type" data-error="Sorry, you have to select one." required>
                                            <option value="">Select</option>
                                            @foreach (getExpenditureTypeList() as $value)
                                                <option value="{{ $value }}" {{ old('expenditure_type') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="help-block with-errors text-danger" >
                                        @if ($errors->has('expenditure_type'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('expenditure_type') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row left-content">
                                <div class="form-group col-sm-6 has-feedback">
                                    <label for="inputInvAmt" class="col-sm-3 control-label">Invoice Amount:</label>
                                    <div class="input-group col-sm-9">
                                        <input type="text" id="inputInvAmt" class="form-control" name="invoice_amount" value="{{ old('invoice_amount') }}"
                                               required>
                                    </div>
                                    <div class="help-block with-errors text-danger" >
                                        @if ($errors->has('invoice_amount'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('invoice_amount') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-sm-6 has-feedback">
                                    <label for="inputMOP" class="col-sm-3 control-label">Mode of Payment:</label>
                                    <div class="input-group col-sm-9">
                                        <select id="inputMOP" class="form-control" name="payment_mode" data-error="Sorry, you have to select one." required>
                                            <option value="">Select</option>
                                            @foreach (getPaymentModeList() as $value)
                                                <option value="{{ $value }}" {{ old('payment_mode') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="help-block with-errors text-danger" >
                                        @if ($errors->has('payment_mode'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('payment_mode') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row left-content">
                                <div class="form-group col-sm-9 has-feedback">
                                    <label for="inputInvAmtW" class="col-sm-2 control-label">Invoice Amount (Words):</label>
                                    <div class="input-group col-sm-10">
                                        <input type="text" id="inputInvAmtW" class="form-control" name="invoice_amount_w" value="{{ old('invoice_amount_w') }}"
                                               required>
                                    </div>
                                    <div class="help-block with-errors text-danger" >
                                        @if ($errors->has('invoice_amount_w'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('invoice_amount_w') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-sm-3 has-feedback">
                                    <label for="inputCurrency" class="col-sm-3 control-label">Currency:</label>
                                    <div class="input-group col-sm-9">
                                        <select id="inputCurrency" class="form-control" name="currency" data-error="Sorry, you have to select one." required>
                                            <option value="">Select</option>
                                            @foreach (getCurrencyList() as $value)
                                                <option value="{{ $value }}" {{ old('currency') == $value ? 'selected' : ''}}>{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="help-block with-errors text-danger" >
                                        @if ($errors->has('currency'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('currency') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row left-content">
                                <div class="form-group col-sm-11 has-feedback">
                                    <label for="inputDescription" class="col-sm-2 control-label">Detailed Description:</label>
                                    <div class="input-group col-sm-10">
                                        <textarea class="form-control" name="description" id="inputDescription"
                                                  placeholder="Write your meassage" required>{{ old('description')}}</textarea>
                                    </div>
                                    <div class="help-block with-errors text-danger" >
                                        @if ($errors->has('description'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row small-spacing">
                                <div class="col-md-12 col-xs-12">
                                    <div class="table-responsive table-purchases">
                                        <table class="table table-bordered table-striped margin-bottom-10">
                                            <thead>
                                            <tr>
                                                <th>NB</th>
                                                <th>Terms(%)</th>
                                                <th>Payment Date</th>
                                                <th>Amount Payable</th>
                                                <th>Account Description</th>
                                                <th>Account Code</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><a href="#" id="inline-nb" data-type="number" data-value="1" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Amount Payable"></a> </td>
                                                    <td><a href="#" id="inline-terms" data-type="number" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Amount Payable"></a> </td>
                                                    <td><a href="#" id="inline-payment-date" data-type="combodate" data-value="21-11-2020" data-format="DD-MM-YYYY" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY" data-pk="1"  data-title="Select Payment Date"></a></td>
                                                    <td><a href="#" id="inline-amount" data-type="number" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Amount Payable">{{$paymentrequest->amount}}</a></td>
                                                    <td><a href="#" id="inline-description" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Description">{{$paymentrequest->description}}</a></td>
                                                    <td><a href="#" id="inline-firstname" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Account Code"></a></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><a href="#" id="inline-amount" data-type="number" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Amount Payable">{{$paymentrequest->amount}}</a></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row small-spacing">
                                <div class="col-md-12 col-xs-12">
                                    <div class="table-responsive table-purchases">
                                        <table class="table table-bordered margin-bottom-10">
                                            <thead>
                                            <tr>
                                                <th>Requesting Div</th>
                                                <th>Prepared By</th>
                                                <th>Head of Finance</th>
                                                <th>Budget Dept</th>
                                                <th>Managing Director</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Rep: <a href="#" id="inline-request-div" data-type="text" data-value="1" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Requesting Division"></a></td>
                                                    <td>Name: <a href="#" id="inline-prepared" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Prepared By"></a></td>
                                                    <td>Name: <a href="#" id="inline-HOF" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Head of Finance"></a></td>
                                                    <td>Name: <a href="#" id="inline-Budget" data-type="number" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Budget Department"></a></td>
                                                    <td style="border: none;">Signature: </td>
                                                </tr>
                                                <tr>
                                                    <td>Signature: </td>
                                                    <td>Signature: </td>
                                                    <td>Signature: </td>
                                                    <td>Signature: </td>
                                                    <td style="border: none;"></td>
                                                </tr><tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td>date: </td>
                                                    <td>date: </td>
                                                    <td>date: </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>                                        <!-- /.box-content card -->
                                </div>
                            </div>
                            <div class="row box-title " style="border-bottom-style: inherit;">
                                <h4 class="" style="color: black;">For Accounts Department Use Only</h4>
                            </div>
                            <div class="row small-spacing">
                                <div class="col-md-12 col-xs-12">
                                    <div class="table-responsive table-purchases">
                                        <table class="table table-bordered table-striped margin-bottom-10">
                                            <thead>
                                            <tr>
                                                <th>Payment JV No.</th>
                                                <th>Invoice Amount</th>
                                                <th>Less Taxes</th>
                                                <th>Net Payable</th>
                                                <th>Bank</th>
                                                <th>Cheque No.</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @for( $i=0 ; $i<4; $i++ )
                                            <tr>
                                                <td>.</td>
                                                <td style="background-color: #cbcbcb"> </td>
                                                <td style="background-color: #4f496d"> </td>
                                                <td style="background-color: #4b4b4b"> </td>
                                                <td> </td>
                                                <td> </td>
                                            </tr>
                                            </tbody>
                                            @endfor
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button onclick="printDiv()" id="remove" class="btn btn-primary waves-effect waves-light">Print</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        function printDiv() {
            var content = document.getElementById('print').outerHTML;
            var mywindow = window.open('', 'Print');

            mywindow.document.write('<html><head><title>Print</title>');
            mywindow.document.write('<link rel="stylesheet" type="text/css" href="/assets/plugin/bootstrap/css/bootstrap.css">');
            mywindow.document.write('<link rel="stylesheet" type="text/css" href="/assets/plugin/bootstrap/css/bootstrap-theme.css">');
            mywindow.document.write('<link rel="stylesheet" type="text/css" href="/assets/styles/style.min.css">');
            mywindow.document.write('<link rel="stylesheet" type="text/css" href="/assets/styles/color/yellow.min.css">');

            mywindow.document.write('</head><body style="min-width: 1024px;" >');
            mywindow.document.write(content);
            mywindow.document.write('</body></html>');

            mywindow.document.close();
            mywindow.focus();

            var element = mywindow.document.getElementById("remove");
            element.parentNode.removeChild(element);

            mywindow.document.onreadystatechange = function () {
                if(mywindow.document.readyState === 'ready' || mywindow.document.readyState === 'complete'){
                    mywindow.print();
                    // mywindow.close();
                }

            };
            return true;
        }
    </script>

    <!-- X-Editable -->
    <script src="{{ asset('assets/plugin/x-editable/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/x-editable.demo.js') }}"></script>
    @endsection
