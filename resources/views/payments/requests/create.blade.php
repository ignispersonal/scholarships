@extends('layouts.beneficiary.main')

@section('page_title')
   Payment Request
@endsection

@section('custom_css')
    <!-- Datepicker -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/datepicker/css/bootstrap-datepicker.min.css') }}">
@endsection


@section('content')
    <div class="row small-spacing">
        <form data-toggle="validator" method="post" action="{{ route('paymentrequests.store') }}" enctype="multipart/form-data">
            @csrf

            <div class="col-md-7 col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-content bordered bordered-all primary margin-bottom-20 ">
                            <h4 class="box-title">Request For Payment</h4>
                            <!-- /.box-title -->

                            <div class="form-group has-feedback">
                                <label for="inputTitle" class="control-label">Title</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user ico"></i></span>
                                    <input type="text" pattern="^[_A-z]{1,}(\w*|\s*|-|\d*)*" maxlength="30" class="form-control"
                                           id="inputLname" name="title" placeholder="title of request" data-error="Sorry, enter a valid value"
                                           value="{{ old('title') }}" required>
                                </div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors">
                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="description" class="control-label">Enter description of payment request</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                    <textarea id="description" class="form-control" name="description" maxlength="225" rows="2" data-error="This field is required"
                                              placeholder="This textarea has a limit of 225 chars.">{{old('description') }}</textarea>
                                </div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors">
                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="inputVendor" class="control-label">Vendor Institution</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user ico"></i></span>
                                    <input type="text" pattern="^[_A-z]{1,}(\w*|\s*|-|\d*)*" maxlength="30" class="form-control"
                                           id="inputVendor" name="vendor_institution" placeholder="name of vendor company." data-error="Sorry, you can't leave this blank"
                                           value="{{ old('vendor_institution') }}" required>
                                </div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors">
                                    @if ($errors->has('vendor_institution'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('vendor_institution') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-6 has-feedback">
                                    <label for="inputDateDue" class="control-label">Date Due</label>
                                    <div class="input-group datepicker" id="inputDateDue">
                                        <span class="input-group-addon"><i class="fa fa-calendar ico"></i></span>
                                        <input data-minlength="6" class="form-control" id="datepicker"
                                               name="date_due" placeholder="dd/mm/yyyy"
                                               value="{{ old('date_due') }}" required>
                                    </div>
                                    <div class="help-block with-errors" >
                                        @if ($errors->has('date_due'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('date_due') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-sm-6 has-feedback">
                                    <label for="inputAmount" class="control-label">Amount</label>
                                    <div class="input-group" id="inputAmount">
                                        <span class="input-group-addon"><i class="fa fa-money ico"></i></span>
                                        <input min="10" class="form-control"
                                               name="amount"
                                               value="{{ old('amount') }}" required>
                                    </div>
                                    <div class="help-block with-errors" >
                                        @if ($errors->has('amount'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('amount') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="exampleInputFile">Upload Supporting Document</label>
                                <input type="file" id="exampleInputFile" name="attached_document">
                                <p class="help-block">file size must not exceed 1MB.</p>
                                <div class="help-block with-errors" >
                                    @if ($errors->has('attached_document'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('attached_document') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                            </div>

                        </div>
                        <!-- /.box-content card -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
        </form>

    </div>
@endsection
@section('scripts')

    <!-- Datepicker -->
    <script src="{{ asset('assets/plugin/datepicker/js/bootstrap-datepicker.min.js') }}"></script>

    <!-- Validator -->
    <script src="{{ asset('assets/plugin/validator/validator.min.js') }}"></script>

    <!-- Demo Scripts -->
    <script src="{{ asset('assets/scripts/form.demo.js') }}"></script>
@endsection
