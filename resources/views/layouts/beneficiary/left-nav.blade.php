<div class="main-menu">
    <header class="header">
        <a href="{{route('root')}}" class="logo">MTN SCHOLARSHIPS</a>
        <button type="button" class="button-close fa fa-times js__menu_close"></button>
        <div class="user">
            <a href="#" class="avatar"><img src="http://placehold.it/80x80" alt=""><span class="status online"></span></a>
            <h5 class="name"><a href="{{route('profile')}}">Rexford Nyarko</a></h5>
            <h5 class="position">Beneficiary</h5>
            <!-- /.name -->
            <div class="control-wrap js__drop_down">
                <i class="fa fa-caret-down js__drop_down_button"></i>
                <div class="control-list">
                    <div class="control-item"><a href="{{route('profile')}}"><i class="fa fa-user"></i> Profile</a></div>
                    <div class="control-item"><a href="#"><i class="fa fa-gear"></i> Settings</a></div>
                    <div class="control-item"><a href="#"><i class="fa fa-sign-out"></i> Log out</a></div>
                </div>
                <!-- /.control-list -->
            </div>
            <!-- /.control-wrap -->
        </div>
        <!-- /.user -->
    </header>
    <!-- /.header -->
    <div class="content">

        <div class="navigation">
            <h5 class="title">Navigation</h5>
            <!-- /.title -->
            <ul class="menu js__accordion">
                <li class="{{ isActiveRoute('/' ,'current active') }}">
                    <a class="waves-effect" href="{{ url('/') }}"><i class="menu-icon fa fa-home"></i><span>Dashboard</span></a>
                </li>
                <li class="{{ isActiveRoute('beneficiary/paymentsrequests*' ,'current active') }}">
                    <a class="waves-effect parent-item js__control" href="#"><i
                            class="menu-icon fa fa-money"></i><span>Payments</span><span
                            class="menu-arrow fa fa-angle-down"></span></a>
                    <ul class="sub-menu js__content">
                        <li class="{{ isActiveRoute('beneficiary/paymentrequests') }}"><a href="{{route('paymentrequests.index')}}">Payments History</a></li>
                        <li class="{{ isActiveRoute('beneficiary/paymentrequests/create') }}"><a href="{{route('paymentrequests.create')}}">Request Payments</a></li>
                    </ul>
                    <!-- /.sub-menu js__content -->
                </li><li class="{{ isActiveRoute('reports*' ,'current active') }}">
                    <a class="waves-effect parent-item js__control" href="#"><i
                            class="menu-icon fa fa-bar-chart"></i><span>Reports</span><span
                            class="menu-arrow fa fa-angle-down"></span></a>
                    <ul class="sub-menu js__content">
                        <li class="{{ isActiveRoute('reports/disbursements') }}"><a href="{{route('disbursements')}}">Disbursements</a></li>
                    </ul>
                    <!-- /.sub-menu js__content -->
                </li>
            </ul>
        </div>
        <!-- /.navigation -->
    </div>
    <!-- /.content -->
</div>
