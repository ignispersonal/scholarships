@extends('layouts.beneficiary.main')

@section('page_title')
    Dashboard
@endsection

@section('content')
    <div class="row small-spacing">

        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content">
                <h4 class="box-title">Payment Requests</h4>
                <!-- /.box-title -->
                <div class="dropdown js__drop_down">
                    <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                    <ul class="sub-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else there</a></li>
                        <li class="split"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    <!-- /.sub-menu -->
                </div>
                <!-- /.dropdown js__dropdown -->
                <div class="content widget-stat">
                    <div class="percent bg-warning"><i class="fa fa-line-chart"></i>53%</div>
                    <!-- /.percent -->
                    <div class="right-content">
                        <h2 class="counter">1</h2>
                        <!-- /.counter -->
                        <p class="text">Pending</p>
                        <!-- /.text -->
                    </div>
                    <!-- /.right-content -->
                    <div class="clear"></div>
                    <!-- /.clear -->
                    <div class="process-bar">
                        <div class="bar-bg bg-warning"></div>
                        <div class="bar js__width bg-warning" data-width="70%"></div>
                        <!-- /.bar js__width bg-success -->
                    </div>
                    <!-- /.process-bar -->
                </div>
                <!-- /.content widget-stat -->
            </div>
            <!-- /.box-content -->
        </div>

        <!-- /.col-lg-3 col-md-6 col-xs-12 -->
        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content">
                <h4 class="box-title">Fees Paid</h4>
                <!-- /.box-title -->
                <div class="dropdown js__drop_down">
                    <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                    <ul class="sub-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else there</a></li>
                        <li class="split"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    <!-- /.sub-menu -->
                </div>
                <!-- /.dropdown js__dropdown -->
                <div class="content widget-stat">
                    <div class="percent bg-danger"><i class="fa fa-line-chart"></i>+40%</div>
                    <!-- /.percent -->
                    <div class="right-content">
                        <h2 class="counter">GHc 2700</h2>
                        <!-- /.counter -->
                        <p class="text">Fees Paid This Year</p>
                        <!-- /.text -->
                    </div>
                    <!-- /.right-content -->
                    <div class="clear"></div>
                    <!-- /.clear -->
                    <div class="process-bar">
                        <div class="bar-bg bg-danger"></div>
                        <div class="bar js__width bg-danger" data-width="70%"></div>
                        <!-- /.bar js__width bg-success -->
                    </div>
                    <!-- /.process-bar -->
                </div>
                <!-- /.content widget-stat -->
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-lg-3 col-md-6 col-xs-12 -->
        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content">
                <h4 class="box-title">Allowances Paid</h4>
                <!-- /.box-title -->
                <div class="dropdown js__drop_down">
                    <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                    <ul class="sub-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else there</a></li>
                        <li class="split"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    <!-- /.sub-menu -->
                </div>
                <!-- /.dropdown js__dropdown -->
                <div class="content widget-stat-chart">
                    <div class="c100 p76 small blue js__circle">
                        <span>76%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <!-- /.c100 p58 -->
                    <div class="right-content">
                        <h2 class="counter">Ghc 1200</h2>
                        <!-- /.counter -->
                        <p class="text">Allowances paid this year</p>
                        <!-- /.text -->
                    </div>
                    <!-- /.right-content -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-lg-3 col-md-6 col-xs-12 -->

        <div class="col-lg-4 col-xs-12">
            <div class="box-content">
                <h4 class="box-title">Performance</h4>
                <!-- /.box-title -->
                <div class="dropdown js__drop_down">
                    <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                    <ul class="sub-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else there</a></li>
                        <li class="split"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    <!-- /.sub-menu -->
                </div>
                <!-- /.dropdown js__dropdown -->
                <div class="content">
                    Upload Semester Transcripts
                </div>
                <!-- /.content -->
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-lg-4 col-md-12 -->

        <div class="col-lg-4 col-xs-12">
            <div class="box-content">
                <h4 class="box-title">Bills</h4>
                <!-- /.box-title -->
                <div class="dropdown js__drop_down">
                    <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                    <ul class="sub-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else there</a></li>
                        <li class="split"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    <!-- /.sub-menu -->
                </div>
                <!-- /.dropdown js__dropdown -->
                <div class="content">
                    Upload Invoices / Expenditures
                </div>
                <!-- /.content -->
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-lg-4 col-md-12 -->
    </div>
@endsection
