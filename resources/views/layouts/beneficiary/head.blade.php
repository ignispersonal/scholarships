<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('page_title') | {{ config('app.name', 'MTN Foundation Scholarships') }}</title>

    <!-- Main Styles -->

    <link rel="stylesheet" href="{{ asset('assets/plugin/bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugin/bootstrap/css/bootstrap-theme.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/styles/style.min.css') }}">
{{--    <link rel="stylesheet" href="{{ asset('assets/styles/style-dark.min.css') }}">--}}

    <!-- mCustomScrollbar -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css') }}">

    <!-- Waves Effect -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/waves/waves.min.css') }}">

    <!-- Sweet Alert -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/sweet-alert/sweetalert.css') }}">

    <!-- Percent Circle -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/percircle/css/percircle.css') }}">

    <!-- Chartist Chart -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/chart/chartist/chartist.min.css') }}">

    <!-- FullCalendar -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugin/fullcalendar/fullcalendar.print.css') }}" media='print'>
    <link rel="stylesheet" href="{{ asset('assets/styles/color/yellow.min.css') }}">

    <style>
        .table > tbody > tr > td {
            vertical-align: middle;
        }
        td .avatar img {
            border: 3px solid #d7ecfb;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%;
            transition: all 0.4s ease;
            -moz-transition: all 0.4s ease;
            -o-transition: all 0.4s ease;
            -ms-transition: all 0.4s ease;
            -webkit-transition: all 0.4s ease;
        }
        form > li > button {
            background: inherit;
            border: none;
            margin: 0 auto;
        }
        form > li > button a:hover {
            color:#ea4335 ;
        }
    </style>
    {{--custom css--}}
    @yield('custom_css')
</head>

    @yield('body')

</html>
