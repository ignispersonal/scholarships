<div class="main-menu">
    <header class="header">
        <a href="{{route('root')}}" class="logo">MTN SCHOLARSHIPS</a>
        <button type="button" class="button-close fa fa-times js__menu_close"></button>
        <div class="user">
            <a href="#" class="avatar"><img src="http://placehold.it/80x80" alt=""><span class="status online"></span></a>
            <h5 class="name"><a href="{{route('profile')}}">EDWARD BRYAN</a></h5>
            <h5 class="position">Administrator</h5>
            <!-- /.name -->
            <div class="control-wrap js__drop_down">
                <i class="fa fa-caret-down js__drop_down_button"></i>
                <div class="control-list">
                    <div class="control-item"><a href="{{route('profile')}}"><i class="fa fa-user"></i> Profile</a></div>
                    <div class="control-item"><a href="#"><i class="fa fa-gear"></i> Settings</a></div>
                    <div class="control-item"><a href="#"><i class="fa fa-sign-out"></i> Log out</a></div>
                </div>
                <!-- /.control-list -->
            </div>
            <!-- /.control-wrap -->
        </div>
        <!-- /.user -->
    </header>
    <!-- /.header -->
    <div class="content">

        <div class="navigation">
            <h5 class="title">Navigation</h5>
            <!-- /.title -->
            <ul class="menu js__accordion">
                <li class="{{ isActiveRoute('/' ,'current active') }}">
                    <a class="waves-effect" href="{{ url('/') }}"><i class="menu-icon fa fa-home"></i><span>Dashboard</span></a>
                </li>
                <li class="{{ isActiveRoute('scholarships*' ,'current active') }}">
                    <a class="waves-effect parent-item js__control" href="#"><i
                            class="menu-icon fa fa-file-text"></i><span>Scholarships</span><span
                            class="menu-arrow fa fa-angle-down"></span></a>
                    <ul class="sub-menu js__content">
                        <li class="{{ isActiveRoute(array('scholarships/batches*')) }}"><a href="{{route('batches.index')}}">Manage Batches</a></li>
                        <li class="{{ isActiveRoute(array('scholarships/types','scholarships/types/create','scholarships/types/*/edit')) }}"><a href="{{route('types.index')}}">Scholarships Types</a></li>
                        <li class="{{ isActiveRoute('scholarships/applications/create') }}"><a href="{{route('applications.create')}}">New Application</a></li>
                        <li class="{{ isActiveRoute('scholarships/applications') }}"><a href="{{ route('applications.index') }}">Manage Applications</a></li>
                        {{--                        <li class="{{ isActiveRoute('scholarships/create') }}"><a href="#">Open Applications</a></li>--}}
                    </ul>
                    <!-- /.sub-menu js__content -->
                </li>

                <li class="{{ isActiveRoute('reports*' ,'current active') }}">
                    <a class="waves-effect parent-item js__control" href="#"><i
                            class="menu-icon fa fa-bar-chart"></i><span>Reports</span><span
                            class="menu-arrow fa fa-angle-down"></span></a>
                    <ul class="sub-menu js__content">
                        <li class="{{ isActiveRoute(array('reports/beneficiaries','reports/beneficiaries/profile')) }}"><a href="{{route('beneficiaries')}}">Beneficiaries</a></li>
                        <li class="{{ isActiveRoute('beneficiary/paymentrequests') }}"><a href="{{route('paymentrequests.index')}}">Payments Requests</a></li>
                        <li class="{{ isActiveRoute('reports/disbursements') }}"><a href="{{route('disbursements')}}">Disbursements</a></li>
{{--                        <li class="{{ isActiveRoute('reports/disbursments/allowances') }}"><a href="#">Allowances</a></li>--}}
{{--                        <li class="{{ isActiveRoute('reports/disbursments/fees') }}"><a href="#">Fees</a></li>--}}
                    </ul>
                    <!-- /.sub-menu js__content -->
                </li>
            </ul>
            <!-- /.menu js__accordion -->
            <!--<h5 class="title">Users</h5>-->
            <!-- /.title -->
            <ul class="menu js__accordion">
{{--                <li class="{{ Request::is('users') ? "current active" : '' }}">--}}
                <li class="{{ isActiveRoute('users*' ,'current active') }}">
                    <a class="waves-effect parent-item js__control" href="#"><i
                            class="menu-icon fa fa-users"></i><span>Users</span><span
                            class="menu-arrow fa fa-angle-down"></span></a>
                    <ul class="sub-menu js__content">
                        <li class="{{ isActiveRoute('users/create') }}"><a href="{{ route('users.create') }}">Add Users</a></li>
                        <li class="{{ isActiveRoute('users') }}"><a href="{{ route('users.index') }}">Manage Users</a></li>
                    </ul>
                    <!-- /.sub-menu js__content -->
                </li>
                <li class="{{ isActiveRoute('alerts','current active') }}">
                    <a class="waves-effect" href="#"><i class="menu-icon fa fa-code"></i><span>Alerts</span><span
                            class="notice notice-yellow">6</span></a>
                </li>
            </ul>
            <!-- /.menu js__accordion -->
        </div>
        <!-- /.navigation -->
    </div>
    <!-- /.content -->
</div>
