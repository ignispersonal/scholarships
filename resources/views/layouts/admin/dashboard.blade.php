@extends('layouts.admin.main')

@section('page_title')
    Dashboard
@endsection

@section('content')
    <div class="row small-spacing">

        <div class="col-xs-12">
            <div class="box-content">
                <h4 class="box-title">Activity</h4>
                <!-- /.box-title -->
                <div class="dropdown js__drop_down">
                    <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                    <ul class="sub-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else there</a></li>
                        <li class="split"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    <!-- /.sub-menu -->
                </div>
                <!-- /.dropdown js__dropdown -->
                <div id="smil-animation-index-chartist-chart" class="chartist-chart" style="height: 320px"></div>
                <!-- /#smil-animation-chartist-chart.chartist-chart -->
            </div>
            <!-- /.box-content -->
        </div>

        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content">
                <h4 class="box-title">New Applications</h4>
                <!-- /.box-title -->
                <div class="dropdown js__drop_down">
                    <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                    <ul class="sub-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else there</a></li>
                        <li class="split"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    <!-- /.sub-menu -->
                </div>
                <!-- /.dropdown js__dropdown -->
                <div class="content widget-stat">
                    <div class="percent bg-warning"><i class="fa fa-line-chart"></i>53%</div>
                    <!-- /.percent -->
                    <div class="right-content">
                        <h2 class="counter">837</h2>
                        <!-- /.counter -->
                        <p class="text">Applicants</p>
                        <!-- /.text -->
                    </div>
                    <!-- /.right-content -->
                    <div class="clear"></div>
                    <!-- /.clear -->
                    <div class="process-bar">
                        <div class="bar-bg bg-warning"></div>
                        <div class="bar js__width bg-warning" data-width="70%"></div>
                        <!-- /.bar js__width bg-success -->
                    </div>
                    <!-- /.process-bar -->
                </div>
                <!-- /.content widget-stat -->
            </div>
            <!-- /.box-content -->
        </div>

        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content">
                <h4 class="box-title">Current Beneficiaries</h4>
                <!-- /.box-title -->
                <div class="dropdown js__drop_down">
                    <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                    <ul class="sub-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else there</a></li>
                        <li class="split"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    <!-- /.sub-menu -->
                </div>
                <!-- /.dropdown js__dropdown -->
                <div class="content widget-stat-chart">
                    <div class="c100 p94 small green js__circle">
                        <span>92%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <!-- /.c100 p58 -->
                    <div class="right-content">
                        <h2 class="counter">3922</h2>
                        <!-- /.counter -->
                        <p class="text">Awardees</p>
                        <!-- /.text -->
                    </div>
                    <!-- /.right-content -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-lg-3 col-md-6 col-xs-12 -->

        <!-- /.col-lg-3 col-md-6 col-xs-12 -->
        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content">
                <h4 class="box-title">Fees Paid</h4>
                <!-- /.box-title -->
                <div class="dropdown js__drop_down">
                    <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                    <ul class="sub-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else there</a></li>
                        <li class="split"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    <!-- /.sub-menu -->
                </div>
                <!-- /.dropdown js__dropdown -->
                <div class="content widget-stat">
                    <div class="percent bg-danger"><i class="fa fa-line-chart"></i>+40%</div>
                    <!-- /.percent -->
                    <div class="right-content">
                        <h2 class="counter">976</h2>
                        <!-- /.counter -->
                        <p class="text">Fees Paid This Month</p>
                        <!-- /.text -->
                    </div>
                    <!-- /.right-content -->
                    <div class="clear"></div>
                    <!-- /.clear -->
                    <div class="process-bar">
                        <div class="bar-bg bg-danger"></div>
                        <div class="bar js__width bg-danger" data-width="70%"></div>
                        <!-- /.bar js__width bg-success -->
                    </div>
                    <!-- /.process-bar -->
                </div>
                <!-- /.content widget-stat -->
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-lg-3 col-md-6 col-xs-12 -->
        <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="box-content">
                <h4 class="box-title">Allowances Paid</h4>
                <!-- /.box-title -->
                <div class="dropdown js__drop_down">
                    <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                    <ul class="sub-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else there</a></li>
                        <li class="split"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    <!-- /.sub-menu -->
                </div>
                <!-- /.dropdown js__dropdown -->
                <div class="content widget-stat-chart">
                    <div class="c100 p76 small blue js__circle">
                        <span>76%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <!-- /.c100 p58 -->
                    <div class="right-content">
                        <h2 class="counter">804</h2>
                        <!-- /.counter -->
                        <p class="text">Beneficiaries Paid</p>
                        <!-- /.text -->
                    </div>
                    <!-- /.right-content -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-lg-3 col-md-6 col-xs-12 -->

        <div class="col-lg-4 col-xs-12">
            <div class="box-content">
                <h4 class="box-title">Statistics</h4>
                <!-- /.box-title -->
                <div class="dropdown js__drop_down">
                    <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                    <ul class="sub-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else there</a></li>
                        <li class="split"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    <!-- /.sub-menu -->
                </div>
                <!-- /.dropdown js__dropdown -->
                <div class="content">
                    <div id="chart-2" class="js__chart" data-type="column"
                         data-chart="'Year'/'Statistics' | '2010'/75 | '2011'/42 | '2012'/75 | '2013'/38 | '2014'/19 | '2015'/93 "></div>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-lg-4 col-md-12 -->

        <div class="col-lg-4 col-xs-12">
            <div class="box-content">
                <h4 class="box-title">Total Projects</h4>
                <!-- /.box-title -->
                <div class="dropdown js__drop_down">
                    <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                    <ul class="sub-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else there</a></li>
                        <li class="split"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    <!-- /.sub-menu -->
                </div>
                <!-- /.dropdown js__dropdown -->
                <div class="content">
                    <div id="chart-3" class="js__chart" data-type="curve"
                         data-chart="'Year'/'Desktop'/'Mobile' | '2008'/53/0 | '2009'/35/73 | '2010'/89/14 | '2011'/50/50 | '2012'/86/37 | '2013'/47/89 | '2014'/75/50 | '2015'/100/70 "></div>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-lg-4 col-md-12 -->

        <div class="col-lg-4 col-xs-12">
            <div class="box-content">
                <h4 class="box-title">Total Disbursement</h4>
                <!-- /.box-title -->
                <div class="dropdown js__drop_down">
                    <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                    <ul class="sub-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else there</a></li>
                        <li class="split"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    <!-- /.sub-menu -->
                </div>
                <!-- /.dropdown js__dropdown -->
                <div class="content">
                    <div id="chart-1" class="js__chart" data-type="donut"
                         data-chart="'Type'/'Number' | 'Legacy Scholarship beneficiaries'/50 | '10million Scholarship beneficiaries'/20 | '20th Anniversary beneficiaries'/20 |'Teacher Improvement Award Programme (TIAP)'/10"></div>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-lg-4 col-xs-12 -->

    </div>
@endsection
