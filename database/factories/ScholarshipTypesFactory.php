<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\ScholarshipTypes;
use Faker\Generator as Faker;

$factory->define(ScholarshipTypes::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->word,
        'description' => $faker->paragraph,
    ];
});
