<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\EducationLevel;
use Faker\Generator as Faker;

$factory->define(EducationLevel::class, function (Faker $faker) {
    return [
        //
        'level' => $faker->word,
        'description' => $faker->paragraph,
    ];
});
