<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Applications;
use App\Batches;
use App\User;
use Faker\Generator as Faker;

$factory->define(Applications::class, function (Faker $faker) {
    return [
        'id'=> $faker->unique()->randomElement(User::doesntHave('applications')->get('id')->pluck('id')),
        'email' => $faker->unique()->safeEmail,
        'fname' => $faker->firstName,
        'lname' => $faker->lastName,
        'date_of_birth' => $faker->dateTimeInInterval('-20 years', '-10 years')->format('d/m/Y'),
        'nationality'=>$faker->country,
        'gender'=>$faker->randomElement(['male','female']),
        'photo' => '/img/unknown.jpg',
        'employment_status' => $faker->randomElement(['employed','unemployed','student','self-employed']),
        'marital_status' => $faker->randomElement(['married','single','divorced','separated','widowed']),
        'status'    => $faker->randomElement([Applications::STATUS_INCOMPLETE,Applications::STATUS_PENDING]),
        'batches_id'  => $faker->randomElement(Batches::all('id')->pluck('id')),
        'institution' => 'University of Ghana',
        'programme' => $faker->randomElement(['Mathematics','Biology','Theatre Arts']),
        'programme_year' => $faker->randomElement([1,2,3,4])
    ];
});
