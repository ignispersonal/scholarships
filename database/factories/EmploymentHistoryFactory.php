<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Applications;
use App\EmploymentHistory;
use Faker\Generator as Faker;

$factory->define(EmploymentHistory::class, function (Faker $faker) {
    return [
        //
        'id' => $faker->uuid,
        'company_name' => $faker->company,
        'job_title'=> $faker->jobTitle,
        'start_date' => $start = $faker->date(),
        'end_date'=> $faker->dateTimeInInterval($start,'+2years'),
        'user_id'=> $faker->randomElement(Applications::has('employmentHistories', '<', 4)->get('id')->pluck('id')),
    ];
});
