<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Applications;
use App\Contacts;
use App\ContactTypes;
use Faker\Generator as Faker;

$factory->define(Contacts::class, function (Faker $faker) {
    return [
        'id'   => $faker->uuid,
        'name' => $faker->name,
        'phone'=> $faker->phoneNumber,
        'email'=> $faker->email,
        'address' => $faker->address,
        'city' => $faker->city,
        'country' => $faker->country,
        'employment_status'=> $faker->randomElement(['employed','unemployed','student','self-employed']),
        'relation' => $faker->randomElement(['uncle','boss','sister','brother']),
        'user_id'=> $faker->randomElement(Applications::get('id')->pluck('id')),
        'contact_type'=> $faker->randomElement(Contacts::contactTypes()),
    ];
});
