<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\ContactTypes;
use Faker\Generator as Faker;

$factory->define(ContactTypes::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->word,
        'desciption' => $faker->word,
    ];
});
