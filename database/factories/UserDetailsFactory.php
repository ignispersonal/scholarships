<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\UserDetails;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(UserDetails::class, function (Faker $faker) {
    return [
        'id'=> $faker->uuid,
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'fname' => $faker->firstName,
        'lname' => $faker->lastName,
        'date_of_birth' => $faker->date(),
        'city'=> $faker->city,
        'country'=>$faker->country,
        'gender'=>$faker->randomElement(['male','female']),
        'photo' => '/img/unknown.jpg',
        'employment_status' => $faker->randomElement(['employed','unemployed','student','self-employed']),
        'marital_status' => $faker->randomElement(['married','single','divorced','separated','widowed']),
        'interests' => $faker->paragraph,
    ];
});
