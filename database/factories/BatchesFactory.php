<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Batches;
use App\ScholarshipTypes;
use Faker\Generator as Faker;

$factory->define(Batches::class, function (Faker $faker) {


    return [
        //
        'id' => $faker->uuid,
        'title' => $faker->unique()->firstName,
        'description' => $faker->paragraph,
        'start_date' => $faker->dateTimeInInterval('-2 years', now())->format('d/m/Y'),
        'end_date'  => $faker->dateTimeInInterval(now(),'+2 years')->format('d/m/Y'),
        'no_applied' => 0,
        'no_awarded' => 0,
        'scholarship_type_id' => $faker->unique()->randomElement( ScholarshipTypes::all('id')->pluck('id')),
        'status' => $faker->randomElement([Batches::CLOSED,Batches::COMPLETED,Batches::OPEN,Batches::ONGOING]),
    ];
});
