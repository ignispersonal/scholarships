<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Applications;
use App\EducationHistory;
use App\EducationLevel;
use Faker\Generator as Faker;

$factory->define(EducationHistory::class, function (Faker $faker) {

    return [
        //
        'id' => $faker->uuid,
        'institute' => $faker->company,
        'educational_level_id' => $level = $faker->randomElement(EducationLevel::all('id')->pluck('id')),
        'certificate_title'=> $faker->jobTitle,
        'certificate_document'=> 'docs/completion1.pdf',
        'start_date' => $start = $faker->date(),
        'end_date'=> $faker->dateTimeInInterval($start,'+4years'),
        'user_id'=> $faker->randomElement(Applications::whereHas('educationHistories',function ($q) use ($level) {
            $q->where('educational_level_id','!=',$level);
        }, '<', 4)->get('id')->pluck('id')),
    ];
});
