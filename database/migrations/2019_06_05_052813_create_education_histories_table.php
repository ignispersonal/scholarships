<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education_histories', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('institute');
            $table->integer('educational_level_id')->unsigned();
            $table->string('certificate_title');
            $table->string('certificate_document');
            $table->date('start_date');
            $table->date('end_date');
            $table->uuid('user_id');
            $table->timestamps();
            $table->softDeletes();

//            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('educational_level_id')->references('id')->on('education_levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education_histories');
    }
}
