<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterApplicationsAddFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('applications', function (Blueprint $table) {
            $table->string('transcript')->nullable();
            $table->string('wassce')->nullable();
            $table->string('adm_letter')->nullable();
            $table->string('testimonial')->nullable();
            $table->string('citizenship')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('applications', function (Blueprint $table) {
            //
            $table->dropColumn('transcript');
            $table->dropColumn('wassce');
            $table->dropColumn('adm_letter');
            $table->dropColumn('testimonial');
            $table->dropColumn('citizenship');
        });
    }
}
