<?php

use App\EducationHistory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;


class EducationHistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        EducationHistory::truncate();
        Schema::enableForeignKeyConstraints();
        factory(EducationHistory::class, 30)->create();
    }
}
