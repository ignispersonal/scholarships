<?php

use App\Batches;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;


class BatchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Batches::truncate();
        Schema::enableForeignKeyConstraints();
        factory(Batches::class, 4)->create();
    }
}
