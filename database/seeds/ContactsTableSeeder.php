<?php

use App\Contacts;
use Illuminate\Database\Seeder;


class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Contacts::class, 30)->create();
    }
}
