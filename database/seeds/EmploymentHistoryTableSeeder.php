<?php

use App\Applications;
use App\EmploymentHistory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;


class EmploymentHistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        EmploymentHistory::truncate();
        Schema::enableForeignKeyConstraints();
        factory(EmploymentHistory::class, 2000)->create();
    }
}
