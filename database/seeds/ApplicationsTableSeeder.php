<?php

use App\Applications;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;


class ApplicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Applications::truncate();
        Schema::enableForeignKeyConstraints();
        factory(Applications::class, 10)->create();
    }
}
