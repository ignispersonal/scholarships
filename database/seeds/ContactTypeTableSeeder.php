<?php

use App\ContactTypes;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;


class ContactTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        ContactTypes::truncate();
        Schema::enableForeignKeyConstraints();

        $ContactTypes = array(
            array('name' => 'Personal Contact', 'description' => 'Contact details of applicant',),
            array('name' => 'Guardian Contact', 'description' => 'Contact details of guardian',),
            array('name' => 'Next Of Kin', 'description' => 'contac details of next of kin. to be contacted in emergency',),
            array('name' => 'Reference', 'description' => 'contact of someone to endorse or confirm ...',),
        );

        foreach ($ContactTypes as $type){
            ContactTypes::create($type);
        }

    }
}
