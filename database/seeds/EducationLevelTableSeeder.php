<?php

use App\EducationLevel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;


class EducationLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        EducationLevel::truncate();
        Schema::enableForeignKeyConstraints();

        $Elevels = array(
            array('level' => 'Basic Education', 'description' => 'basic education to read and write, usually grades 1 or primary 1 to JHS 3',),
            array('level' => 'High School', 'description' => 'high school education usually SHS',),
            array('level' => 'Pre College - No degree', 'description' => 'usually refers to certificate, diploma or vocational training education to aquire a skill',),
            array('level' => 'College - degree', 'description' => 'standard college/university bachelors degree',),
            array('level' => 'Post graduate', 'description' => '............',),
            array('level' => 'Masters Degree', 'description' => '............',),
            array('level' => 'Doctorial or Professional degree', 'description' => '...............',),
        );
        // create Rate

        foreach ($Elevels as $level){
            EducationLevel::create($level);
        }

    }
}
