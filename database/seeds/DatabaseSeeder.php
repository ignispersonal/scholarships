<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RolesAndPermissionsTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(ScholarshipTableSeeder::class);
         $this->call(BatchesTableSeeder::class);
         $this->call(ContactTypeTableSeeder::class);
         $this->call(ApplicationsTableSeeder::class);
         $this->call(ContactsTableSeeder::class);
         $this->call(EducationLevelTableSeeder::class);
         $this->call(EmploymentHistoryTableSeeder::class);
         $this->call(EducationHistoryTableSeeder::class);
    }
}
