<?php

use App\ContactTypes;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class RolesAndPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');
        Schema::disableForeignKeyConstraints();
        DB::table('model_has_permissions')->truncate();
        DB::table('model_has_roles')->truncate();
        DB::table('role_has_permissions')->truncate();
        Role::truncate();
        Permission::truncate();
        Schema::enableForeignKeyConstraints();

        // create permissions for user model
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'edit users']);
        Permission::create(['name' => 'view users']);
        Permission::create(['name' => 'delete users']);

        // create permissions for roles model
        Permission::create(['name' => 'create roles']);
        Permission::create(['name' => 'edit roles']);
        Permission::create(['name' => 'view roles']);
        Permission::create(['name' => 'delete roles']);
        Permission::create(['name' => 'assign roles']);
        Permission::create(['name' => 'revoke roles']);

        // create permissions for permissions model
        Permission::create(['name' => 'create permissions']);
        Permission::create(['name' => 'edit permissions']);
        Permission::create(['name' => 'view permissions']);
        Permission::create(['name' => 'delete permissions']);
        Permission::create(['name' => 'assign permissions']);
        Permission::create(['name' => 'revoke permissions']);

        // create permissions for application model
        Permission::create(['name' => 'create application']);
        Permission::create(['name' => 'edit application']);
        Permission::create(['name' => 'view application']);
        Permission::create(['name' => 'delete application']);

        // create permissions for batches model
        Permission::create(['name' => 'create batches']);
        Permission::create(['name' => 'edit batches']);
        Permission::create(['name' => 'view batches']);
        Permission::create(['name' => 'delete batches']);

        // create permissions for scholarship types model
        Permission::create(['name' => 'create scholarship types']);
        Permission::create(['name' => 'edit scholarship types']);
        Permission::create(['name' => 'view scholarship types']);
        Permission::create(['name' => 'delete scholarship types']);

        // create permissions for payment request model
        Permission::create(['name' => 'create payment request']);
        Permission::create(['name' => 'edit payment request']);
        Permission::create(['name' => 'view payment request']);
        Permission::create(['name' => 'delete payment request']);

        // create permissions for rates model
        Permission::create(['name' => 'view reports']);
        Permission::create(['name' => 'view own']);


        // create roles and assign them their peermissions
        $role = Role::create(['name' => 'administrator']);
        $role->givePermissionTo(Permission::all());

        $role = Role::create(['name' => 'editor']);
        $role->givePermissionTo(Permission::all());

        Role::create(['name' => 'manager']);

        Role::create(['name' => 'beneficiary']);

//        $role->givePermissionTo(['create users','edit users','view users','delete users']); //permissions for users
//        $role->givePermissionTo(['create roles','edit roles','view roles','assign roles','revoke roles']); //permissions for roles
//        $role->givePermissionTo(['view permissions','assign permissions','revoke permissions']); //permissions for permissions
//        $role->givePermissionTo(['create services','edit services','view services','delete services']); //permissions for services
//        $role->givePermissionTo(['create stations','edit stations','view stations','delete stations']); //permissions for stations
//        $role->givePermissionTo(['create rates','edit rates','view rates','delete rates']); //permissions for rates
//        $role->givePermissionTo(['create tickets','edit tickets','view tickets','delete tickets']); //permissions for tickets
//        $role->givePermissionTo(['create reports','edit reports','view reports','delete reports']); //permissions for service

    }
}
