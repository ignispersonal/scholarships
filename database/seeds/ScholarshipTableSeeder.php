<?php

use App\ScholarshipTypes;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;


class ScholarshipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        ScholarshipTypes::truncate();
        Schema::enableForeignKeyConstraints();

        $scholarshipTypes = array(
            array('name' => 'Legacy Scholarship beneficiaries', 'description' => 'Legacy Scholarship beneficiaries',),
            array('name' => '10 million Scholarship beneficiaries', 'description' => '10million Scholarship beneficiaries',),
            array('name' => 'Teacher Improvement Award Programme (TIAP)', 'description' => 'Teacher Improvement Award Programme (TIAP)',),
            array('name' => '20th Anniversary beneficiaries', 'description' => '20th Anniversary beneficiaries',),
        );
        // create Rate

        foreach ($scholarshipTypes as $type){
            ScholarshipTypes::create($type);
        }

    }
}
