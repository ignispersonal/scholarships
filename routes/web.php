<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
})->name('root');

Route::get('/apply', 'ApplicationsController@apply')->name('apply');


Route::get('/reports/beneficiaries', function () {
    return view('beneficiaries');
})->name('beneficiaries');

Route::get('/reports/disbursements', function () {
    return view('disbursements');
})->name('disbursements');

Route::get('/reports/disbursement-profile', function () {
    return view('disbursement-profile');
})->name('disbursement-profile');

Route::get('/reports/beneficiaries/profile', function () {
    return view('profile');
})->name('profile');

Route::get('/reports/paymentrequests/{paymentrequest}/generate', 'PaymentRequestController@generate')->name('paymentrequests.generate');



Route::get('dashboard', function () {
    return view('layouts.admin.dashboard');
})->name('admin');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('users','UserController');

Route::resource('scholarships/batches','BatchesController')->except('show');

Route::resource('scholarships/types','ScholarshipTypesController')->except('show');

Route::get('scholarships/applications/{application}/download', 'ApplicationsController@downloadAll')->name('applications.downloadall');

Route::resource('scholarships/applications','ApplicationsController');

Route::resource('beneficiary/paymentrequests','PaymentRequestController');

Route::resource('beneficiary','Beneficiaries\BeneficiaryController');


//Route::resource('payments/paymentrequests','PaymentRequestController');


