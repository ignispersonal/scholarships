<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScholarshipTypes extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * relationship between scholarships and batches
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function batches(){
        return $this->hasMany(Batches::class,'scholarship_type_id');
    }
}
