<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Applications extends Model
{
    use SoftDeletes, UUID;

    protected $dates = ['deleted_at','date_of_birth'];

    const STATUS_INCOMPLETE = 'incomplete';
    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';
    const STATUS_REJECTED = 'rejected';

    const MALE = 'male';
    const FEMALE = 'female';

    const EMP = 'employed';
    const UNEMP = 'unemployed';
    const SELFEMP = 'self employed';
    const STU = 'student';

    const MARRIED = "married";
    const SINGLE = "single";
    const SEPARATED = "separated";
    const DIVORCED = "divorced";
    const WIDOWED = "widowed";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname',
        'lname',
        'email',
        'date_of_birth',
        'nationality',
        'gender',
        'photo',
        'employment_status',
        'marital_status',
        'institution',
        'programme',
        'programme_year',
        'batches_id',
        'status',
        'transcript',
        'wassce',
        'adm_letter',
        'testimonial',
        'citizenship',
    ];


    /**
     * Mutator to set the format the input values for end date
     * @param $end_date
     */
    public function setDateOfBirthAttribute($date_of_birth){

        $this->attributes['date_of_birth'] =  Carbon::createFromFormat('d/m/Y', $date_of_birth)->toDateString();
    }


    public function getFullName(){
        return $this->fname ." ".$this->lname;
    }
    /**
     * Implements relationship between user and application
     *
     * @return BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,'id');
    }

    /**
     * determines whether or not the application is approved
     * @return bool
     */
    public function isAccepted(){
        return $this->status == Applications::STATUS_APPROVED;
    }

    /**
     * determines whether or not the application is completed
     * @return bool
     */
    public function isPending(){
        return $this->status == Applications::STATUS_PENDING;
    }

    /**
     * determines whether or not the application is incomplete
     * @return bool
     */
    public function isInComplete(){
        return $this->status == Applications::STATUS_INCOMPLETE;
    }

    /**
     * determines whether or not the application is incomplete
     * @return bool
     */
    public function isRejected(){
        return $this->status == Applications::STATUS_REJECTED;
    }

    /**
     * determines the color of the application status text
     * @return string
     */
    public function statusColor(){

        if ($this->isInComplete()){
            return "text-warning";
        }elseif ($this->isPending()){
            return "text-info";
        }elseif ($this->isRejected()){
            return "text-danger";
        }elseif ($this->isAccepted()) {
            return "text-success";
        }
    }

    /**
     * return a list of employment status
     * @return array
     */
    public static function empStatus(){
        return array(self::EMP, self::SELFEMP, self::UNEMP, self::STU);
    }

    /**
     * returns a list of marital statuses
     * @return array
     */
    public static function maritalStatus(){
        return array(self::SINGLE, self::MARRIED, self::SEPARATED, self::DIVORCED, self::WIDOWED);
    }

    public static function applicationStatus(){
        return array(self::STATUS_INCOMPLETE, self::STATUS_PENDING, self::STATUS_REJECTED, self::STATUS_APPROVED);
    }
    /**
     * relationship between Batches and application
     *
     * @return BelongsTo
     */
    public function batch(){
        return $this->belongsTo(Batches::class);
    }

    /**
     * Defines relationship between application and educational history
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function educationHistories(){
        return $this->hasMany(EducationHistory::class, 'user_id');
    }

    /**
     * Defines relationship between application and employment history
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employmentHistories(){
        return $this->hasMany(EmploymentHistory::class,'user_id');
    }

    /**
     * Defines relationship between application and contacts
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contacts(){
        return $this->hasMany(Contacts::class,'user_id');
    }

    /**
     * Defines relationship between application and attached documents
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachedDocuments(){
        return $this->hasMany(Contacts::class,'user_id');
    }

}
