<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Batches extends Model
{
    //
    //
    use SoftDeletes, UUID;

    protected $dates = ['deleted_at','start_date','end_date'];

    const OPEN = "taking applications";
    const CLOSED = "closed applications";
    const COMPLETED = "completed";
    const ONGOING = "running";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'start_date',
        'end_date',
        'no_applied',
        'no_awarded',
        'scholarship_type_id',
        'status',
    ];


    /**
     * returns all Batch statuses
     * @return array
     */
    public static function getAllStatusAttribute(){
        return array(self::OPEN,self::CLOSED,self::ONGOING,self::COMPLETED);
    }


    /**
     * Mutator to set the format the input values for end date
     * @param $end_date
     */
    public function setEndDateAttribute($end_date){

        $this->attributes['end_date'] =  Carbon::createFromFormat('d/m/Y', $end_date)->toDateString();
    }


    /**
     * Mutator to set the format the input values for start date
     * @param $start_date
     */
    public function setStartDateAttribute($start_date){

        $this->attributes['start_date'] = Carbon::createFromFormat('d/m/Y', $start_date)->toDateString();
    }

    /**
     * checks if batch is closed for applications
     * @return bool
     */
    public function isClosed(){
        return $this->status == self::CLOSED ? true : false;
    }

    /**
     * checks if batch is open for applications
     * @return bool
     */
    public function isOpen(){
        return $this->status == self::OPEN ? true : false;
    }

    /**
     * checks if batch is completed
     * @return bool
     */
    public function isCompleted(){
        return $this->status == self::COMPLETED ? true : false;
    }

    /**
     * checks if batch is completed
     * @return bool
     */
    public function isOngoing(){
        return $this->status == self::isOngoing() ? true : false;
    }

    /**
     * Relationship between batches and scholarships
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function scholarshipType(){
        return $this->belongsTo(ScholarshipTypes::class, 'scholarship_type_id');
    }

    /**
     * Relationship between batches and user details
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userDetails(){
        return $this->hasMany(UserDetails::class);
    }

    /**
     * Relationship between batches and user details
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applications(){
        return $this->hasMany(Applications::class);
    }
}
