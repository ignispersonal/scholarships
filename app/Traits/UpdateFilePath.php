<?php
namespace App;
use Illuminate\Http\UploadedFile;

trait UpdateFilePath
{
    protected static function boots()
    {
        parent::boot();

        static::updating(function ($model) {
            foreach ($model->toArray() as $key => $attribute){
                if($attribute instanceof UploadedFile){

                    $model->{$key} = $attribute->storeAs($model->getTable().'/'.$model->id, $key.'-'.time().'.'. $attribute->extension());;
                }
            }
            dd($model);
        });
    }

}
