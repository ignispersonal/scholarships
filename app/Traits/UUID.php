<?php
namespace App;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

trait UUID
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string) Str::uuid();

            foreach ($model->toArray() as $key => $attribute){
                if($attribute instanceof UploadedFile){
                    $model->{$key} = $attribute->storeAs($model->getTable().'/'.$model->id, $key.'-'.time().'.'. $attribute->extension());;
                }
            }
        });

        static::updating(function ($model) {
            foreach ($model->toArray() as $key => $attribute){
                if($attribute instanceof UploadedFile){
                    $model->{$key} = $attribute->storeAs($model->getTable().'/'.$model->id, $key.'-'.time().'.'. $attribute->extension());;
                }
            }
        });
    }

    public function getIncrementing()
    {
        return false;
    }
}
