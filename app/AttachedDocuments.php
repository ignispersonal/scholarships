<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttachedDocuments extends Model
{
    use SoftDeletes, UUID;

    const TRANSCRIPT = 'transcript';
    const INVOICE = 'invoice';
    const ADMISSION = 'admission letter';
    const TESTIMONIAL = 'testimonial';
    const CITIZENSHIP = 'citizenship';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'type',
        'path',
        'user_id',
    ];

    public static function getTypes(){
        return array(self::TRANSCRIPT, self::INVOICE, self::ADMISSION, self::TESTIMONIAL, self::CITIZENSHIP);
    }

    /**
     * creates relationship between applications and attached documents
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function application(){
        return $this->belongsTo(Applications::class, 'user_id','id');
    }

}
