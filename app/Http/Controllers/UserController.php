<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::withTrashed()->with('roles')->where('id','!=','1')->paginate(50);

        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles = Role::pluck('name','id');
        return view('users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        //rules for input validation
        $rules = [
//            'username' => ['required','string','max:255', 'unique:users'],
            'fname' => ['required','string','max:255'],
            'lname' => ['required','string','max:255'],
            'photo' => ['image','mimes:jpg,png,jpeg'],
            'email' => ['required','string','email','max:255', 'unique:users'],
            'password' => ['required','string','min:8','confirmed'],
            'role' => ['required','string','max:255','exists:roles,id'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('users/create')
                ->withErrors($validator)
                ->withInput();
        }

        $data = $request->except('role','isActive');

        if ($request->hasFile('photo') && $request->file('photo')->isValid()){
            $data['photo'] = $request->file('photo')->store('img/profile_photos');
        }else{
            $data['photo'] = "img/unknown.png";
        }

        $user = User::create($data);
        $user->assignRole( $request->role );

        return redirect()->route('users.index')->with('success', 'New User Account for, '. $user->fname
            .' created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {
//        return view('users.edit', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::pluck('name','id');
        return view('users.edit', compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
//        dd($request);
        $rules = [
//            'username' => ['required','string','max:255', 'unique:users'],
            'fname' => ['required','string','max:255'],
            'lname' => ['required','string','max:255'],
            'photo' => ['image','mimes:jpg,png,jpeg'],
            'email' => ['required','string','email','max:255', 'unique:users,email,'.$user->id],
            'password' => ['nullable','string','min:8','confirmed'],
            'role' => ['required','string','max:255','exists:roles,id'],
        ];

        $validator = Validator::make($request->all(), $rules);


        if ($validator->fails()) {
            return redirect(route('users.edit',$user->id))
                ->withErrors($validator)
                ->withInput();

        }

/*        if ($request->filled('username')){
            $user->username = $request->username;
        }*/

        if ($request->filled('fname')){
            $user->fname = $request->fname;
        }
        if ($request->filled('lname')){
            $user->lname = $request->lname;
        }
        if ($request->hasFile('photo') && $request->file('photo')->isValid()){
//            Storage::delete('img/profile_photos/'.$user->photo);
            $user['photo'] = $request->file('photo')->store('img/profile_photos');
        }
        if ($request->filled('email') && $user->email != $request->email){
            $user->email = $request->email;
        }
        if ($request->filled('password')){
            $user->password = $request->password;
        }

        $user->syncRoles(array($request->role));

//        if (!$user->isDirty()){
//            return redirect()->route('users.index')->with('info', 'No changes made');
//        }

        $user->save();

        return redirect()->route('users.index')->with('success', 'User Account for '. $user->fname
            .' updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
