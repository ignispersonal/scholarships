<?php

namespace App\Http\Controllers;

use App\PaymentRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PaymentRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paymentRequests = PaymentRequest::query();
        //if current user is beneficiary, showing only their records
        if(Auth::user()->hasRole('beneficiary')){
            $paymentRequests = $paymentRequests->where('user_id','=',Auth::id())->get();
        }
        $paymentRequests = $paymentRequests->all();

        return view('payments.requests',compact('paymentRequests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('payments.requests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'title' => ['required','string','max:255'],
            'description'=> ['required','string','max:255'],
            'amount' => ['required','numeric','between:0,50000.99'],
            'vendor_institution' => ['required','string','max:255'],
            'date_due' => ['required','date_format:d/m/Y','after_or_equal:today'],
            'attached_document' => ['required','mimes:jpg,png,jpeg,pdf'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->route('paymentrequests.create')
                ->withErrors($validator)
                ->withInput();
        }

        $paymentRequest = new PaymentRequest;
        $paymentRequest->fill($request->only(
            'title',
            'description',
            'amount',
            'vendor_institution',
            'date_due'
        ));

        $paymentRequest->status = PaymentRequest::PENDING;
        $paymentRequest->attached_document = $request->file('attached_document')->store('payment_requests/'.$paymentRequest->id);;
        $paymentRequest->user_id = Auth::user()->id;
        $paymentRequest->save();

        return redirect()->route('paymentrequests.index')->with('success', 'New Payment Request, '. $paymentRequest->id
            .' created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentRequest $paymentRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentRequest $paymentrequest)
    {//
        if(Auth::user()->hasRole('beneficiary') && !$paymentrequest->isPending()){
            return redirect()->route('paymentrequests.index')->with('info', 'Cannot edit payment request that is '.$paymentrequest->status);
        }
        return view('payments.requests.edit', compact('paymentrequest'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentRequest  $paymentrequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentRequest $paymentrequest)
    {
        //
        $rules = [
            'title' => ['required','string','max:255'],
            'description'=> ['required','string','max:255'],
            'amount' => ['required','numeric','between:0,50000.99'],
            'vendor_institution' => ['required','string','max:255'],
            'date_due' => ['required','date_format:d/m/Y','after_or_equal:today'],
            'attached_document' => ['mimes:jpg,png,jpeg,pdf'],
            'status' => ['string','max:255','in:'.implode(',',PaymentRequest::getAllStatus())],

        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->route('paymentrequests.edit',$paymentrequest->id)
                ->withErrors($validator)
                ->withInput();
        }

        $paymentrequest->fill($request->only(
            'title',
            'description',
            'amount',
            'vendor_institution',
            'date_due',
            'status'
        ));

        if($request->hasFile('attached_document') && $request->file('attached_document')->isValid()){
            $paymentrequest->attached_document = $request->file('attached_document')->store('payment_requests/'.$paymentrequest->id);;
        }

        if (!$paymentrequest->isDirty()){
            return redirect()->route('paymentrequests.edit',$paymentrequest->id)->with('info', 'No changes made');
        }
        $paymentrequest->save();

        return redirect()->route('paymentrequests.index')->with('success', 'New Payment Request, '. $paymentrequest->id
            .' updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentRequest $paymentRequest)
    {
        //
    }

    public function generate(PaymentRequest $paymentrequest){
        return view('payments.generate',compact('paymentrequest'));
    }
}
