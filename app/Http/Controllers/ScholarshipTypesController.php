<?php

namespace App\Http\Controllers;

use App\ScholarshipTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ScholarshipTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $scholarships = ScholarshipTypes::withTrashed()->get();
        return view('scholarshipTypes.index',compact('scholarships'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('scholarshipTypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //rules for input validation
        $rules = [
            'name' => ['required','string','max:255','unique:scholarship_types'],
            'description' => ['required','string'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->route('types.create')
                ->withErrors($validator)
                ->withInput();
        }

        $scholarship = ScholarshipTypes::create($request->only('name','description'));

        if (!$request->filled('isActive')){
            $scholarship->delete();
        }

        return redirect()->route('types.index')->with('success', 'New Scholarship Type, '. $scholarship->name
        .' created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ScholarshipTypes  $scholarshipTypes
     * @return \Illuminate\Http\Response
     */
    public function show(ScholarshipTypes $scholarshipTypes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ScholarshipTypes  $scholarshipTypes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $scholarship = ScholarshipTypes::withTrashed()->findOrFail($id);
        return view('scholarshipTypes.edit',compact('scholarship'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ScholarshipTypes  $scholarshipTypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //rules for input validation
        $rules = [
            'name' => ['required','string','max:255','unique:scholarship_types,name,'.$id],
            'description' => ['required','string'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->route('types.edit',$id)
                ->withErrors($validator)
                ->withInput();
        }

        $scholarship = ScholarshipTypes::withTrashed()->findOrFail($id);

        if($request->filled('name')){
            $scholarship->name = $request->name;
        }

        if ($request->filled('description')){
            $scholarship->description = $request->description;
        }

        if (($request->filled('isActive')) && ($scholarship->deleted_at != null) ){
            $scholarship->deleted_at = null;

        }elseif($scholarship->deleted_at == null && !$request->filled('isActive')){
            $scholarship->deleted_at = now();
        }

        if(!$scholarship->isDirty()){
            return redirect()->route('types.edit',$id)->with('info','No Changes Made To '.$scholarship->name);
        };

        $scholarship->save();
        return redirect()->route('types.index')->with('success', ''. $scholarship->name
            .' updated successfully.');
    }

    /**
     * disable, enable or Remove the specified resource from storage.
     *
     * @param  \App\ScholarshipTypes  $scholarshipTypes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $type = ScholarshipTypes::withTrashed()->findOrFail($id);
        if($type->deleted_at != null){
            $type->restore();
            return redirect()->route('types.index')->with('info', 'Enabled scholarship type '. $type->name
                .'. New batches can now be created under this type again.');
        }

        if($type->batches()->exists()){
            $type->delete();
            return redirect()->route('types.index')->with('info', 'Disabled scholarship type '. $type->name
                .'. New batches cannot be created under this type anymore, existing batches will remain.');
        }

        $type->forceDelete();
        return redirect()->route('types.index')->with('success', 'scholarship type, '. $type->name
            .' deleted successfully.');
    }
}
