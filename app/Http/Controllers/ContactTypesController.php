<?php

namespace App\Http\Controllers;

use App\ContactTypes;
use Illuminate\Http\Request;

class ContactTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContactTypes  $contactTypes
     * @return \Illuminate\Http\Response
     */
    public function show(ContactTypes $contactTypes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContactTypes  $contactTypes
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactTypes $contactTypes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContactTypes  $contactTypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactTypes $contactTypes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactTypes  $contactTypes
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactTypes $contactTypes)
    {
        //
    }
}
