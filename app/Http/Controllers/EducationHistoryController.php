<?php

namespace App\Http\Controllers;

use App\EducationHistory;
use Illuminate\Http\Request;

class EducationHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EducationHistory  $educationHistory
     * @return \Illuminate\Http\Response
     */
    public function show(EducationHistory $educationHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EducationHistory  $educationHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(EducationHistory $educationHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EducationHistory  $educationHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EducationHistory $educationHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EducationHistory  $educationHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(EducationHistory $educationHistory)
    {
        //
    }
}
