<?php

namespace App\Http\Controllers;

use App\Applications;
use App\Batches;
use App\Contacts;
use App\ScholarshipTypes;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use UnexpectedValueException;
use ZipArchive;

class ApplicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $applications = Applications::join('batches','applications.batches_id','=','batches.id')
            ->join('scholarship_types','batches.scholarship_type_id','=','scholarship_types.id')
            ->select('applications.*','scholarship_types.name as scholarshiptype')->get();

        $scholarshipTypes = ScholarshipTypes::withTrashed()->pluck('name','id');
        return view('applications.index',compact('applications','scholarshipTypes'));
    }


    /**
     * Display a application page.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadAll(Applications $application)
    {
        //

        $zip_file = $application->id .'.zip';
        $zip = new ZipArchive();
        $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        $path = storage_path('app/public/'.$application->getTable().'/'.$application->id);

        try{
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));
        }catch (UnexpectedValueException $error){
            abort(404);
        };
        foreach ($files as $name => $file)
        {
            // We're skipping all subfolders
            if (!$file->isDir()) {
                $filePath     = $file->getRealPath();

                // extracting filename with substr/strlen
                $relativePath = substr($filePath, strlen($path) + 1);

                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();
        return response()->download($zip_file);

    }

    /**
     * Display a application page.
     *
     * @return \Illuminate\Http\Response
     */
    public function apply()
    {
        //
        $batches = Batches::with('scholarshipType')->where('status','=',Batches::OPEN)->get();

        return view('applications.apply',compact('batches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $batches = Batches::where('status','=',Batches::OPEN)->pluck('title','id');
        return view('applications.create',compact('batches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $phoneRegex = "/^\(?(\d{3})\)?[\s-]?\d{3}[\s-]?\d{4}$/";
        $rules = [
            'fname' => ['required','string','max:255'],
            'lname' => ['required','string','max:255'],
            'date_of_birth' => ['required','date_format:d/m/Y','after_or_equal:01/01/1969'],
            'nationality' => ['required','string','in:'.implode(',',getNationalityList())],
            'gender' => ['required','string','max:6','min:4'],
            'photo' => ['required','image','mimes:jpg,png,jpeg'],
            'employment_status' => ['required','string','in:'.implode(',',Applications::empStatus())],
            'marital_status' => ['required','string','in:'.implode(',',Applications::maritalStatus())],
            'institution' =>['required','string','max:255'],
            'programme' => ['required','string','max:255'],
            'programme_year' => ['required','integer','max:8','min:1'],
            'batches_id' => ['required','string','max:255','exists:batches,id'],

            //validation for personal contact
            'email' => ['required','string','max:255','regex:/^.+@.+$/i'],
            'phone' => ['required','string', "regex:$phoneRegex"],
            'address' =>['required','string','max:255'],
            'city' => ['required','string','max:255'],
            'country' => ['required','string','in:'.implode(',',getCountryList())],

            //validation for gaurdian contact
            'gname' => ['required','string','max:255'],
            'gemail' => ['required','string','max:255','regex:/^.+@.+$/i'],
            'gphone' => ['required','string', "regex:$phoneRegex"],
            'gaddress' =>['required','string','max:255'],
            'gcity' => ['required','string','max:255'],
            'gcountry' => ['required','string','in:'.implode(',',getCountryList())],
            'relation' => ['required','string','max:255'],
            'gemp' => ['required','string','in:'.implode(',',Applications::empStatus())],

            'transcript' => ['required','mimes:pdf'],
            'wassce' => ['required','mimes:pdf'],
            'adm_letter' => ['required','mimes:pdf'],
            'testimonial' => ['required','mimes:pdf'],
            'citizenship' => ['required','mimes:pdf'],

        ];

        $validator = Validator::make($request->all(), $rules);

        if (!Auth::check()) {
            if ($validator->fails()) {
                return redirect()->route('apply')
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        if ($validator->fails()) {
            return redirect()->route('applications.create')
                ->withErrors($validator)
                ->withInput();
        }

        $application = new Applications;
        $application->fill( $request->only(
            'fname',
            'lname',
            'email',
            'date_of_birth',
            'nationality',
            'gender',
            'photo',
            'employment_status',
            'marital_status',
            'institution',
            'programme',
            'programme_year',
            'batches_id',
            'transcript',
            'wassce',
            'adm_letter',
            'testimonial',
            'citizenship'
        ));

        $application->status = Applications::STATUS_PENDING;
        $application->save();
        $application = $application->refresh();



        $pContactData = $request->only(
            'phone',
            'email',
            'address',
            'city',
            'country',
            'employment_status');
        $pContactData['name'] = $application->getFullName();
        $pContactData['relation'] = Contacts::RELATION_SELF;
        $pContactData['contact_type'] = Contacts::PERSONAL;
        $pContactData['user_id'] = $application->id;

        Contacts::create($pContactData);

        $gContactData['name'] = $request->gname;
        $gContactData['phone'] = $request->gphone;
        $gContactData['email'] = $request->gemail;
        $gContactData['address'] = $request->gaddress;
        $gContactData['city'] = $request->gcity;
        $gContactData['country'] = $request->gcountry;
        $gContactData['relation'] = $request->relation;
        $gContactData['employment_status'] = $request->gemp;
        $gContactData['contact_type'] = Contacts::GUARDIAN;
        $gContactData['user_id'] = $application->id;

        Contacts::create($gContactData);



        return redirect()->route('applications.index')->with('success', 'New application, '. $application->id
            .' created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Applications  $applications
     * @return \Illuminate\Http\Response
     */
    public function show(Applications $applications)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Applications  $applications
     * @return \Illuminate\Http\Response
     */
    public function edit(Applications $application)
    {
        //
        $batches = Batches::pluck('title','id');
        $gContact = null;
        foreach (Contacts::where('user_id','=',$application->id)->get() as $contact){
            $contact->contact_type == Contacts::GUARDIAN ? $gContact = $contact: null;
            $contact->contact_type == Contacts::PERSONAL ? $pContact = $contact: null;
        }

        return view('applications.edit',compact('batches','application','gContact','pContact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Applications  $application
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Applications $application)
    {
        //
        $phoneRegex = "/^\(?(\d{3})\)?[\s-]?\d{3}[\s-]?\d{4}$/";
        $rules = [
            'fname' => ['required','string','max:255'],
            'lname' => ['required','string','max:255'],
            'date_of_birth' => ['required','date_format:d/m/Y','after_or_equal:01/01/1969'],
            'nationality' => ['required','string','in:'.implode(',',getNationalityList())],
            'gender' => ['required','string','max:6','min:4'],
            'photo' => ['image','mimes:jpg,png,jpeg'],
            'employment_status' => ['required','string','in:'.implode(',',Applications::empStatus())],
            'marital_status' => ['required','string','in:'.implode(',',Applications::maritalStatus())],
            'institution' =>['required','string','max:255'],
            'programme' => ['required','string','max:255'],
            'programme_year' => ['required','integer','max:8','min:1'],
            'batches_id' => ['required','string','max:255','exists:batches,id'],

            //validation for personal contact
            'email' => ['required','string','max:255','regex:/^.+@.+$/i'],
            'phone' => ['required','string', "regex:$phoneRegex"],
            'address' =>['required','string','max:255'],
            'city' => ['required','string','max:255'],
            'country' => ['required','string','in:'.implode(',',getCountryList())],

            //validation for gaurdian contact
            'gname' => ['required','string','max:255'],
            'gemail' => ['required','string','max:255','regex:/^.+@.+$/i'],
            'gphone' => ['required','string', "regex:$phoneRegex"],
            'gaddress' =>['required','string','max:255'],
            'gcity' => ['required','string','max:255'],
            'gcountry' => ['required','string','in:'.implode(',',getCountryList())],
            'relation' => ['required','string','max:255'],
            'gemp' => ['required','string','in:'.implode(',',Applications::empStatus())],

            //validation for forms
            'transcript' => ['mimes:pdf'],
            'wassce' => ['mimes:pdf'],
            'adm_letter' => ['mimes:pdf'],
            'testimonial' => ['mimes:pdf'],
            'citizenship' => ['mimes:pdf'],

            'status' => ['required','string','max:255','in:'.implode(',',Applications::applicationStatus())],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->route('applications.edit', $application->id)
                ->withErrors($validator)
                ->withInput();
        }

        $application->fname = $request->fname;
        $application->lname = $request->lname;
        $application->email = $request->email;
        $application->date_of_birth = $request->date_of_birth;
        $application->nationality = $request->nationality;
        $application->gender = $request->gender;
        $application->employment_status = $request->employment_status;
        $application->marital_status = $request->marital_status;
        $application->institution = $request->institution;
        $application->programme = $request->programme;
        $application->batches_id = $request->batches_id;
        $application->status = $request->status;

        if ($request->hasFile('photo') && $request->file('photo')->isValid()){
//            Storage::delete('img/profile_photos/'.$user->photo);
            $application->photo = $request->photo;
        }
        //dd($application);
        if ($application->isDirty()){
            $application->save();
            $application = $application->refresh();
        }

        $pContactData = $request->only(
            'phone',
            'email',
            'address',
            'city',
            'country',
            'employment_status');
        $pContactData['name'] = $application->getFullName();
        $pContactData['relation'] = Contacts::RELATION_SELF;
        $pContactData['contact_type'] = Contacts::PERSONAL;
        $pContactData['user_id'] = $application->id;

        Contacts::where('user_id','=',$application->id)->where('contact_type','=',Contacts::PERSONAL)->update($pContactData);

        $gContactData['name'] = $request->gname;
        $gContactData['phone'] = $request->gphone;
        $gContactData['email'] = $request->gemail;
        $gContactData['address'] = $request->gaddress;
        $gContactData['city'] = $request->gcity;
        $gContactData['country'] = $request->gcountry;
        $gContactData['relation'] = $request->relation;
        $gContactData['employment_status'] = $request->gemp;
        $gContactData['contact_type'] = Contacts::GUARDIAN;
        $gContactData['user_id'] = $application->id;

        Contacts::where('user_id','=',$application->id)->where('contact_type','=',Contacts::GUARDIAN)->update($gContactData);



        return redirect()->route('applications.index')->with('success', 'Application, '. $application->id
            .' updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Applications  $applications
     * @return \Illuminate\Http\Response
     */
    public function destroy(Applications $applications)
    {
        //
    }
}
