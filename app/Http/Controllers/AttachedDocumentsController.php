<?php

namespace App\Http\Controllers;

use App\AttachedDocuments;
use Illuminate\Http\Request;

class AttachedDocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AttachedDocuments  $attachedDocuments
     * @return \Illuminate\Http\Response
     */
    public function show(AttachedDocuments $attachedDocuments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AttachedDocuments  $attachedDocuments
     * @return \Illuminate\Http\Response
     */
    public function edit(AttachedDocuments $attachedDocuments)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AttachedDocuments  $attachedDocuments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AttachedDocuments $attachedDocuments)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AttachedDocuments  $attachedDocuments
     * @return \Illuminate\Http\Response
     */
    public function destroy(AttachedDocuments $attachedDocuments)
    {
        //
    }
}
