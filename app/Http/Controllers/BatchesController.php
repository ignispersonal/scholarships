<?php

namespace App\Http\Controllers;

use App\Batches;
use App\ScholarshipTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BatchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $batches = Batches::with(array('scholarshipType'=>function($query){$query->withTrashed()->select('id','name');}))->withTrashed()->get();

        return view('batches.index',compact('batches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $scholarshipTypes = ScholarshipTypes::pluck('name','id');
        return view('batches.create',compact('scholarshipTypes'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //rules for input validation
        $rules = [
            'title' => ['required','string','max:255'],
            'description' => ['required','string','max:255'],
            'start_date' => ['required','date_format:d/m/Y','after_or_equal:today'],
            'end_date' => ['required','date_format:d/m/Y','max:255','after:start_date'],
            'scholarship_type_id' => ['required','integer','exists:scholarship_types,id'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->route('batches.create')
                ->withErrors($validator)
                ->withInput();
        }

        $data = $request->only('title', 'description', 'start_date', 'end_date', 'scholarship_type_id');
        $batch = Batches::create($data);

        return redirect()->route('batches.index')->with('success', 'New scholarship batch, '. $batch->title
            .' created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Batches  $batches
     * @return \Illuminate\Http\Response
     */
    public function show(Batches $batch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Batches  $batches
     * @return \Illuminate\Http\Response
     */
    public function edit(Batches $batch)
    {
        //
        $scholarshipTypes = ScholarshipTypes::withTrashed()->pluck('name','id');
        return view('batches.edit',compact('batch','scholarshipTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Batches  $batches
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Batches $batch)
    {
        //rules for input validation
        $rules = [
            'title' => ['required','string','max:255'],
            'description' => ['required','string','max:255'],
            'start_date' => ['required','date_format:d/m/Y','after_or_equal:today'],
            'end_date' => ['required','date_format:d/m/Y','max:255','after:start_date'],
            'scholarship_type_id' => ['required','integer','exists:scholarship_types,id'],
            'status' => ['required','string','in:'.Batches::ONGOING.','.Batches::COMPLETED.','.Batches::OPEN.','.Batches::CLOSED],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->route('batches.edit',$batch->id)
                ->withErrors($validator)
                ->withInput();
        }

        $batch->title = $request->title;
        $batch->description = $request->description;
        $batch->scholarship_type_id = $request->scholarship_type_id;
        $batch->start_date = $request->start_date;
        $batch->end_date = $request->end_date;
        $batch->status = $request->status;


        if (!$batch->isDirty()){
            return redirect()->route('batches.edit',$batch->id)->with('info', 'No changes made');
        }

        $batch->save();

        return redirect()->route('batches.index')->with('success', 'scholarship batch, '. $batch->title
            .' updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Batches  $batches
     * @return \Illuminate\Http\Response
     */
    public function destroy(Batches $batch)
    {
        //
        if($batch->applications()->exists() || $batch->userDetails()->exists()){
            return redirect()->route('batches.index')->with('info', 'cannot delete scholarship batch, '. $batch->title
                .' has at least one application or beneficiary');
        }

        $batch->forceDelete();
        return redirect()->route('batches.index')->with('success', 'scholarship batch, '. $batch->title
            .' deleted successfully.');
    }
}
