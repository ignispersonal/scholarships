<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentRequest extends Model
{

    use SoftDeletes, UUID;

    protected $dates = ['deleted_at','date_due'];

    const PENDING = "pending";
    const PROCESSING = "processing";
    const PAID = "paid";
    const CANCELLED = "cancelled";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'amount',
        'vendor_institution',
        'date_due',
        'user_id',
        'attached_document',
        'status',
    ];


    /**
     * Mutator to set the format the input values for end date
     * @param $end_date
     */
    public function setDateDueAttribute($date_due){
        $this->attributes['date_due'] =  Carbon::createFromFormat('d/m/Y', $date_due)->toDateString();
    }

    /**
     * returns true if status is pending
     * @return bool
     */
    public function isPending(){
        return $this->status == self::PENDING ? true : false;
    }

    /**
     * returns true if status is processing
     * @return bool
     */
    public function isProcessing(){
        return $this->status == self::PROCESSING ? true : false;
    }

    /**
     * returns true if status is paid
     * @return bool
     */
    public function isPaid(){
        return $this->status == self::PAID ? true : false;
    }

    /**
     * returns true if status is cancelled
     * @return bool
     */
    public function isCancelled(){
        return $this->status == self::CANCELLED ? true : false;
    }

    /**
     * determines the color of the payment status text
     * @return string
     */
    public function statusColor(){

        if ($this->isPending()){
            return "text-warning";
        }elseif ($this->isProcessing()){
            return "text-info";
        }elseif ($this->isCancelled()){
            return "text-danger";
        }elseif ($this->isPaid()) {
            return "text-success";
        }
    }

    /**
     * returns array of payment request status
     * @return array
     */
    public static function getAllStatus(){
        return array(self::PENDING,self::PROCESSING,self::PAID,self::CANCELLED);
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

}
