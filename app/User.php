<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, UUID, HasRoles, SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password','photo','fname','lname',
//        'username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     *Mutator to encrypt passwords in model to avoid rewriting functions in other controllers
     *
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    /**
     * Defines relationship between user account and applications
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function applications(){
        return $this->hasOne(Applications::class, 'id');
    }

    /**
     * Defines relationship between user account and user details
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userDetails(){
        return $this->hasOne(UserDetails::class, 'id');
    }

    /**
     * Defines relationship between user account and educational history
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function educationHistories(){
        return $this->hasMany(EducationHistory::class);
    }

    /**
     * Defines relationship between user account and employment history
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employmentHistories(){
        return $this->hasMany(EmploymentHistory::class);
    }

    /**
     * Defines relationship between user account and contacts
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contacts(){
        return $this->hasMany(Contacts::class);
    }


}
