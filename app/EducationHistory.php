<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EducationHistory extends Model
{
    //
    use SoftDeletes, UUID;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'institute',
        'education_level_id',
        'certificate_title',
        'certificate_document',
        'start_date',
        'end_date',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime',
    ];

    /**
     * creates relationship between user and educational history
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * creates relationship between userdetails and educational history
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userDetail(){
        return $this->belongsTo(UserDetails::class,'user_id','id');
    }

    /**
     * creates relationship between applications and educational history
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function application(){
        return $this->belongsTo(Applications::class, 'user_id','id');
    }

    /**
     * creates relationship between educationlevel  and educational history
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function educationLevel(){
        return $this->belongsTo(EducationLevel::class);
    }

}
