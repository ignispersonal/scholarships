<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDetails extends Model
{
    use SoftDeletes, UUID;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'fname',
            'lname',
            'date_of_birth',
            'city',
            'country',
            'gender',
            'photo',
            'employment_status',
            'marital_status',
            'interests',
            'batches_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date_of_birth' => 'datetime',
    ];

    /**
     * Implements relationship between user and details
     *
     * @return BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,'id');
    }

    /**
     * relationship between Bathces and user details
     *
     * @return BelongsTo
     */
    public function batch(){
        return $this->belongsTo(Batches::class);
    }

    /**
     * Defines relationship between user details and educational history
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function educationHistories(){
        return $this->hasMany(EducationHistory::class, 'user_id');
    }

    /**
     * Defines relationship between user details and employment history
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employmentHistories(){
        return $this->hasMany(EmploymentHistory::class, 'user_id');
    }

    /**
     * Defines relationship between user details and contacts
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contacts(){
        return $this->hasMany(Contacts::class,'user_id');
    }


}
