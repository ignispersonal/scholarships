<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmploymentHistory extends Model
{
    //
    use SoftDeletes, UUID;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name',
        'job_title',
        'start_date',
        'end_date',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime',
    ];

    /**
     * Implements relationship between user and employmentHistory
     *
     * @return BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }


    /**
     * creates relationship between userdetails and employment history
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userDetail(){
        return $this->belongsTo(UserDetails::class,'user_id','id');
    }

    /**
     * creates relationship between applications and employment history
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function application(){
        return $this->belongsTo(Applications::class, 'user_id','id');
    }
}
