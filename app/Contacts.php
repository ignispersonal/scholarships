<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contacts extends Model
{
    //
    use SoftDeletes, UUID;

    protected $dates = ['deleted_at'];

    const PERSONAL = "Personal Contact";
    const GUARDIAN = "Guardian Contact";
    const RELATION_SELF = "self";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'email',
        'address',
        'city',
        'country',
        'employment_status',
        'relation',
        'contact_type',
        'user_id'
    ];

    public static function contactTypes(){
        return array(self::PERSONAL, self::GUARDIAN);
    }

    /**
     * determines is the contact is a personal contact
     * @return bool
     */
    public function isPersonal(){
        return $this->contact_type == self::PERSONAL ? true : false;
    }

    /**
     * determines is the contact is a personal contact
     * @return bool
     */
    public function isGuardian(){
        return $this->contact_type == self::GUARDIAN ? true : false;
    }

    /**
     * Implements relationship between user and contacts
     *
     * @return BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

//    /**
//     * relationship between contacts and contact type
//     *
//     * @return BelongsTo
//     */
//    public function contactType(){
//        return $this->belongsTo(ContactTypes::class);
//    }


    /**
     * creates relationship between userdetails and contacts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userDetail(){
        return $this->belongsTo(UserDetails::class,'user_id','id');
    }

    /**
     * creates relationship between applications and contacts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function application(){
        return $this->belongsTo(Applications::class, 'user_id','id');
    }

}
