<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EducationLevel extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'level',
        'description',
    ];

    /**
     * relationship betwenn educationlevel and educational history
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function educationHistiories(){
        return $this->hasMany(EducationHistory::class);
    }
}
