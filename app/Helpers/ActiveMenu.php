<?php
    /*
    |--------------------------------------------------------------------------
    | Detect Active Route
    |--------------------------------------------------------------------------
    |
    | Compare given route with current route and return output if they match.
    | Very useful for navigation, marking if the link is active.
    |
    */

    function isActiveRoute($route, $active = "current")
    {
        return Request::is($route) ? $active : '';
    }

    /*
    |--------------------------------------------------------------------------
    | Detect Active Routes
    |--------------------------------------------------------------------------
    |
    | Compare given routes with current route and return output if they match.
    | Very useful for navigation, marking if the link is active.
    |
    */
    function areActiveRoutes(Array $routes, $active = "current")
    {
        foreach ($routes as $route)
        {
            return Request::is($route) ? $active : '';
        }

    }
